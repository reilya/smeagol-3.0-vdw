#!/bin/sh
#
##set -x
#
# Get absolute path of this script, as that will be the Src directory to use
# as reference when copying files.
# 
#
srcdir=$(
cd -P -- "$(dirname -- "$0")" &&
pwd -P
)
# The above construct is more robust than:  srcdir=$(dirname $0)
# (It will work if $0 is "../Src", since we want an *absolute* path
#
user_specified_dir=$(dirname $0)
testdir=$(dirname $srcdir)/Tests
#
destdir=$(pwd)

# SMEAGOL BEGIN
# Creates symboloc links to Interface and NETransport files.
interface=$srcdir/Interface
ln -fs $interface/absdiff.F $srcdir/absdiff.F
ln -fs $interface/bulktrans.F $srcdir/bulktrans.F
ln -fs $interface/dmbk.f $srcdir/dmbk.f
ln -fs $interface/emt2g.f $srcdir/emt2g.f
ln -fs $interface/emt2k.F $srcdir/emt2k.F
ln -fs $interface/emtg.F $srcdir/emtg.F
ln -fs $interface/emtk.F $srcdir/emtk.F
ln -fs $interface/emtrans.F $srcdir/emtrans.F
ln -fs $interface/hsl.f $srcdir/hsl.f
ln -fs $interface/hsleads.f $srcdir/hsleads.f
ln -fs $interface/hslk.f $srcdir/hslk.f
ln -fs $interface/pasbias.f $srcdir/pasbias.f
ln -fs $interface/mountxij.F90 $srcdir/mountxij.F90
ln -fs $interface/shifth.F $srcdir/shifth.F
ln -fs $interface/smeagol_end.F90 $srcdir/smeagol_end.F90
ln -fs $interface/smeagol_options.F90 $srcdir/smeagol_options.F90
ln -fs $interface/vmattr.F $srcdir/vmattr.F
ln -fs $interface/vvbias.F $srcdir/vvbias.F
ln -fs $interface/zhsleads.F $srcdir/zhsleads.F
netransport=$srcdir/NETransport
ln -fs $netransport/chargebias.F90 $srcdir/chargebias.F90
ln -fs $netransport/complex_bands.F $srcdir/complex_bands.F
ln -fs $netransport/const.h $srcdir/const.h
ln -fs $netransport/constF90.h $srcdir/constF90.h
ln -fs $netransport/current.F $srcdir/current.F
ln -fs $netransport/decimate_leads.f $srcdir/decimate_leads.f
ln -fs $netransport/diagonal_alex.f $srcdir/diagonal_alex.f
ln -fs $netransport/electrodes.F $srcdir/electrodes.F
ln -fs $netransport/electrodes-lm.F $srcdir/electrodes-lm.F
ln -fs $netransport/gaucheb.f $srcdir/gaucheb.f
ln -fs $netransport/gauleg.f $srcdir/gauleg.f
ln -fs $netransport/simpson.f $srcdir/simpson.f
ln -fs $netransport/gensvd.F $srcdir/gensvd.F
ln -fs $netransport/get_allselfenergies.F $srcdir/get_allselfenergies.F
ln -fs $netransport/get_energygrid_adapt.F $srcdir/get_energygrid_adapt.F
ln -fs $netransport/get_energygrid_adapt_aux.F $srcdir/get_energygrid_adapt_aux.F
ln -fs $netransport/identify.f $srcdir/identify.f
ln -fs $netransport/invert.f $srcdir/invert.f
ln -fs $netransport/keldysh.F90 $srcdir/keldysh.F90
ln -fs $netransport/leads_cell.F $srcdir/leads_cell.F
ln -fs $netransport/leads_complex.F $srcdir/leads_complex.F
ln -fs $netransport/localdos.F $srcdir/localdos.F
ln -fs $netransport/misc.f $srcdir/misc.f
ln -fs $netransport/negf2g.f $srcdir/negf2g.f
ln -fs $netransport/negf2k.F $srcdir/negf2k.F
ln -fs $netransport/negf_add.F90 $srcdir/negf_add.F90
ln -fs $netransport/negfk.F $srcdir/negfk.F
ln -fs $netransport/negfkleadskp.F $srcdir/negfkleadskp.F
ln -fs $netransport/negfkleadskp-lm.F $srcdir/negfkleadskp-lm.F
ln -fs $netransport/ozaki.F90 $srcdir/ozaki.F90
ln -fs $netransport/rank.F $srcdir/rank.F
ln -fs $netransport/ranksvd.F $srcdir/ranksvd.F
ln -fs $netransport/rho.F $srcdir/rho.F
ln -fs $netransport/rhoG_reduce.F $srcdir/rhoG_reduce.F
ln -fs $netransport/selfenergy.F $srcdir/selfenergy.F
ln -fs $netransport/sigma.F $srcdir/sigma.F
ln -fs $netransport/sorthalf.f $srcdir/sorthalf.f
ln -fs $netransport/transm.F $srcdir/transm.F
ln -fs $netransport/transm_kp.F $srcdir/transm_kp.F
ln -fs $netransport/ErhoG_reduce.F $srcdir/ErhoG_reduce.F
ln -fs $netransport/set_boundary_periodic.F90 $srcdir/set_boundary_periodic.F90
inetransport=$srcdir/INETransport
ln -fs $inetransport/inet.F90 $srcdir/inet.F90
ln -fs $inetransport/inet_check.F90 $srcdir/inet_check.F90
ln -fs $inetransport/inet_current.F90 $srcdir/inet_current.F90
ln -fs $inetransport/inet_distrib.F90 $srcdir/inet_distrib.F90
ln -fs $inetransport/inet_engrid.F90 $srcdir/inet_engrid.F90
ln -fs $inetransport/inet_ephcoupl.F90 $srcdir/inet_ephcoupl.F90
ln -fs $inetransport/inet_greenfunc.F90 $srcdir/inet_greenfunc.F90
ln -fs $inetransport/inet_hilbert.F90 $srcdir/inet_hilbert.F90
ln -fs $inetransport/inet_lead.F90 $srcdir/inet_lead.F90
ln -fs $inetransport/inet_negfk.F90 $srcdir/inet_negfk.F90
ln -fs $inetransport/inet_options.F90 $srcdir/inet_options.F90
ln -fs $inetransport/inet_phselfen.F90 $srcdir/inet_phselfen.F90
ln -fs $inetransport/inet_recipes.F90 $srcdir/inet_recipes.F90
ln -fs $inetransport/inet_spectral.F90 $srcdir/inet_spectral.F90
ln -fs $inetransport/inet_write.F90 $srcdir/inet_write.F90
# SMEAGOL END


#
# Replicate the hierarchy of makefiles
#
(cd $srcdir;
  for i in $(find . -name \[mM\]akefile | grep -v \\./Makefile); do
    relpath=${i%/*}
    mkdir -p ${destdir}/$relpath
    cp $relpath/*akefile ${destdir}/$relpath
  done
)
# Replicate any .h files
# This is needed in some systems with broken include file import heuristics
# (e.g., CSCS blanc)
#
(cd $srcdir;
  for i in $(find . -name '*.h' ); do
    relpath=${i%/*}
    mkdir -p ${destdir}/$relpath
    cp -f $relpath/*.h ${destdir}/$relpath
  done
)
#
sed "s#VPATH=\.#VPATH=${srcdir}#g" ${srcdir}/Makefile > ${destdir}/Makefile

#
# Tests directory
# Create a list of files and use tar to process the list and copy the files
# to the destination directory
#
( cd ${testdir} ; cd .. ; find Tests  \
              -path *Reference -prune -o  \
              -path *Reference-xml -prune -o  \
              -path *work -prune      -o  \
              -path *.arch-ids  -prune -o -print \
              | tar -cf - --no-recursion -T- )   | ( cd ${destdir} ; tar xf -)
#
# Make a symbolic link for the reference output directory
#
(cd ${destdir}/Tests ; ln -sf ${testdir}/Reference ./Reference)
#
echo " *** Compilation setup done. "
echo " *** Remember to copy an arch.make file or run configure as:"
echo "    ${user_specified_dir}/configure [configure_options]"
