C ********* Variables *************************************
	
	IMPLICIT NONE

C ********** Parameters ***********************************
	INTEGER :: N1,nsc(2),
     &   nspinL,maxnhL,nspinR,maxnhR,
     &   Nenerg,Nenerg1,Nenerg2,NPOLES,ITER,istep,ICASE
	INTEGER, SAVE :: N12,NL,NL2,NR,NR2
        INTEGER, SAVE :: N_IVPOINTS,IVSTART,IVFINISH
	INTEGER, SAVE :: Nenerg_div,NenergIm_div,NenergImNode,Nsmall_div
	LOGICAL, SAVE :: PERIODIC=.TRUE.
	
	INCLUDE "const.h"

C ******** Auxiliaries ******************************************************
	DOUBLE COMPLEX, ALLOCATABLE :: Tau1_aux(:,:)
	DOUBLE COMPLEX, ALLOCATABLE :: GF1_aux(:,:), GF2_aux(:,:) 
	DOUBLE COMPLEX, ALLOCATABLE :: Sigma_aux(:,:),GF_iter_dag(:,:)
	DOUBLE COMPLEX, ALLOCATABLE :: GF_iter(:,:),aux(:,:),Ic_aux(:)
	INTEGER,	ALLOCATABLE :: IPIV(:)
	DOUBLE COMPLEX, ALLOCATABLE :: WORK(:)
	DOUBLE COMPLEX, ALLOCATABLE :: Gamma_L(:,:),Gamma_R(:,:)
	DOUBLE COMPLEX, ALLOCATABLE :: Gamma1_aux(:,:),Gamma2_aux(:,:)
	
C ******** Labels ***********************************************************
	CHARACTER slabel*20,slabelL*20,slabelR*20,paste*25,curfile*25,
     .            lgfile*25
	
C ******** Constant arrays **************************************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: Sigma_L(:,:,:),
     .                                       Sigma_R(:,:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: ZSigma_L(:,:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: ZSigma_R(:,:,:)

C ****** Charge Density *****************************************************
	DOUBLE COMPLEX, ALLOCATABLE :: Deltarho(:,:),rho0(:,:)
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: rhoG

C ********** Leads H0,H1,S0,S1 ****************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: H0_L(:,:),H1_L(:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: S0_L(:,:),S1_L(:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: H0_R(:,:),H1_R(:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: S0_R(:,:),S1_R(:,:)

C ********** Decimation Variables *************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE  :: QR(:,:),QL(:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE  :: VHL(:,:),VHR(:,:),
     .                                        VSL(:,:),VSR(:,:)
	INTEGER, SAVE :: numberL,numberR
C ********** Scaterer Hamiltonian *************************
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: H_Chain
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: S0_Chain

C ******* Fermi-Dirac Distributions for Leads 1 and 2 *****
	DOUBLE COMPLEX :: CONST,fL,fR,CONSTL,CONSTR

C ********** Energy Related Variables  ********************

C Energy
	DOUBLE PRECISION :: Ei,Delta,EB
	DOUBLE PRECISION,SAVE :: EnergI,EnergF,Ef_Lead,dE,R0,
     &                                Ef_LeadL,Ef_LeadR
	DOUBLE PRECISION :: Vini,Vfinal
C Temperature
	DOUBLE PRECISION :: T	
C Potencial bias
	DOUBLE PRECISION :: V
	DOUBLE PRECISION :: Ic

	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: EContour(:),WContour(:)
	
        DOUBLE PRECISION :: kpoint(3)
	
	INTEGER :: IV
	INTEGER :: I,J,II,JJ,L,K,iu,iuc,iul,nrchan
	INTEGER :: ISPIN_CASE,IE_CASE,INFO,NSLICES
	INTEGER :: Nnodes,MyNode,Iprime
	LOGICAL :: fileexist,CONVERGED,CALCTRANSM,leadsgf
	DOUBLE PRECISION :: fermi_aux
C ********** Timing ****************************************
	DOUBLE PRECISION :: t0,dtime
	
