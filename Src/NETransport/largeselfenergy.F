! 
! Copyright (c) Smeagol Authors:
! A. R. Rocha, V. Garcia-Suarez, S. Bailey, C. J. Lambert, J. Ferrer and
! S. Sanvito 2003-2005
! 
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
! "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
! LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
! A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
! OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!
! SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE (www.smeagol.tcd.ie)
! UPON COMPLETION OF THE "SMEAGOL ACADEMIC LICENSE" .
!
! FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL: smeagol@tcd.ie
!
      SUBROUTINE 

          GLaux(:,:,ISPIN) = (Ei*S0_L-H0_L(:,:,ISPIN)-
     &         SigmaLaux(:,:,ISPIN))
          Call ZGETRF(nl,nl,GLaux(:,:,ISPIN),nl,IPIV,INFO)
          Call ZGETRI(nl,GLaux(:,:,ISPIN),nl,IPIV,WRK,nl**2,INFO) 

          GRaux(:,:,ISPIN) = (Ei*S0_R-H0_R(:,:,ISPIN)-
     &         SigmaRaux(:,:,ISPIN))
          Call ZGETRF(nR,nR,GRaux(:,:,ISPIN),nR,IPIV,INFO)
          Call ZGETRI(nR,GRaux(:,:,ISPIN),nR,IPIV,WRK,nR**2,INFO) 

C This is crude but, it will work for a 2D system.
       kpoint(2)=kperp/aside
       do iik=1,nk
        do jjk=1,nk
          do jj=1,nl
           do ii=1,nl
             GL((iik-1)*NL+ii,(jjk-1)*NL+jj,:)=
     &       GL((iik-1)*NL+ii,(jjk-1)*NL+jj,:)+
     &      GLaux(ii,jj,:)*CDEXP(zi*(kpoint(1)*
     &       posxyL(ii,jj,1)+
     &       kpoint(2)*(posxyL(ii,jj,2)+
     &        (iik-jjk)*aside)))
           enddo
          enddo

          do jj=1,nr
           do ii=1,nr
             GR((iik-1)*NR+ii,(jjk-1)*NR+jj,:)=
     &       GR((iik-1)*NR+ii,(jjk-1)*NR+jj,:)+
     &      GRaux(ii,jj,:)*CDEXP(zi*(kpoint(1)*
     &       posxyR(ii,jj,1)+
     &       kpoint(2)*(posxyR(ii,jj,2)+
     &        (iik-jjk)*aside)))
           enddo
          enddo
        enddo
       enddo

      enddo

      END SUBROUTINE 
