! 
! Copyright (c) Smeagol Authors:
! A. R. Rocha, V. Garcia-Suarez, S. Bailey, C. J. Lambert, J. Ferrer and
! S. Sanvito 2003-2005
! 
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
! "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
! LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
! A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
! OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!
! SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE (www.smeagol.tcd.ie)
! UPON COMPLETION OF THE "SMEAGOL ACADEMIC LICENSE" .
!
! FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL: smeagol@tcd.ie


      SUBROUTINE get_all_selfenergies(sigma_method,
     &      Nenerg_div,IVSTART,IVFINISH,
     &      N_IVPOINTS,NSPIN, iktot,iik,
     &      side_rankL, numberL, NL, EiCompL,
     &      H0_L,VHL, S0_L,VSL, Sigma_L,QL,nrchanL,
     &      side_rankR, numberR, NR, EiCompR,
     &      H0_R,VHR, S0_R,VSR, Sigma_R,QR,nrchanR)

C *****************************************************************
C Calculates the selfenergies based on generalized SVD method 
C Written by Ivan Rungger, October 2007
C Computational Spintronics Group
C Trinity College Dublin
C e-mail: runggeri@tcd.ie
C ********** HISTORY **********************************************
C Original version:	October 2007
C ********** INPUT ************************************************


      use parallel
#ifdef MPI
      use mpi_siesta
#endif
      IMPLICIT NONE
      
      INTEGER Nenerg_div,IVSTART,IVFINISH,NSPIN,iktot,NL,NR,
     &    nrchanL,nrchanR,I,ISPIN,iik,INFO,N_IVPOINTS,sigma_method
      CHARACTER(LEN=1) :: side_rankL(NSPIN,iktot), 
     &    side_rankR(NSPIN,iktot)
      INTEGER :: numberL(NSPIN,iktot), 
     &    numberR(NSPIN,iktot),MPIerror,Mynode,Nnodes
      DOUBLE COMPLEX :: EiCompL(Nenerg_div), EiCompR(Nenerg_div),
     &    H0_L(NL,NL,NSPIN), S0_L(NL,NL),
     &    VSL(NL,NL,NSPIN,iktot), VHL(NL,NL,NSPIN,iktot),
     &    Sigma_L(NL,NL,Nenerg_div,NSPIN,iktot),
     &    QL(NL,NL,NSPIN,iktot),
     &    H0_R(NR,NR,NSPIN), S0_R(NR,NR),
     &    VSR(NR,NR,NSPIN,iktot), VHR(NR,NR,NSPIN,iktot),
     &    Sigma_R(NR,NR,Nenerg_div,NSPIN,iktot),
     &    QR(NR,NR,NSPIN,iktot)

        CALL TIMER('RE_SENE',1)         

#ifdef MPI
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,mynode,MPIerror)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,Nnodes,MPIerror)
#else
      Nnodes=1
      MyNode=0
#endif




        DO I=IVSTART,IVFINISH

c$$$          write(*,*)"enesl=",DREAL(EiCompL(I)),DIMAG(EiCompL(I))
c$$$          write(*,*)"enesr=",DREAL(EiCompR(I)),DIMAG(EiCompR(I))
          DO ISPIN=1,NSPIN
            CALL SELFENERGY('L',sigma_method,side_rankL(ISPIN,iik),
     &          numberL(ISPIN,iik),
     &          NL,EiCompL(I),H0_L(:,:,ISPIN),VHL(:,:,ISPIN,iik),
     &          S0_L(:,:),VSL(:,:,ISPIN,iik),
     &          Sigma_L(:,:,I-N_IVPOINTS,ISPIN,iik),
     &          QL(:,:,ISPIN,iik),INFO,nrchanL)

            If (INFO .EQ. 1) Then
              Print*, 'The condition number of the matrix H1 
     &            was too large, left lead',EiCompL(I)
              Print*, 'warning:Should stop but I go on, lets see'
            EndIf

C            call geigenvalues1( Sigma_L(:,:,I-N_IVPOINTS,ISPIN,iik),
C     &          zv, NL)

C            do i1=1, NL
C              write(12347,*)"sleigorig=",iik,DREAL(EiCompL(I)),
C     &            DREAL(zv(i1)),DIMAG(zv(i1))
C            enddo

C            CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
C            call writemat8(DREAL(EiCompL(I)),
C     &          sigma_l(:,:,I-N_IVPOINTS,ISPIN,iik),nl,nl,-10D0,
C     &          "sigmalsml")
C            CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
            CALL SELFENERGY('R',sigma_method,side_rankR(ISPIN,iik),
     &            numberR(ISPIN,iik)
     &            ,NR,EiCompR(I),H0_R(:,:,ISPIN),
     &            VHR(:,:,ISPIN,iik),S0_R(:,:),VSR(:,:,ISPIN,iik),
     &            Sigma_R(:,:,I+N_IVPOINTS,ISPIN,iik),
     &            QR(:,:,ISPIN,iik),INFO,nrchanR)


C            call geigenvalues1( Sigma_R(:,:,I+N_IVPOINTS,ISPIN,iik),
C     &          zv, NR)
C
C            do i1=1, NR
C              write(12347,*)"sreigorig=",iik,DREAL(EiCompR(I)),
C     &            DREAL(zv(i1)),DIMAG(zv(i1))
C            enddo






C            CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
C            call writemat8(DREAL(EiCompR(I)),
C     &          sigma_r(:,:,I+N_IVPOINTS,ISPIN,iik),nr,nr,-10D0,
C     &          "sigmarsml")
C            CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
!            call f77flush
 
            If (INFO .EQ. 1) Then
              Print*, 'The condition number of the matrix H1 
     &            was too large, right lead',EiCompR(I)
              Print*, 'warning:Should stop but I go on, lets see'
            EndIf

!        call MPI_Finalize( MPIerror )
!        stop

          ENDDO
        ENDDO

        CALL TIMER('RE_SENE',2)


      end SUBROUTINE get_all_selfenergies

      SUBROUTINE get_all_selfenergies2(Nenerg_div,IVSTART,IVFINISH,
     &      N_IVPOINTS,NSPIN, iktot,iik,
     &      side_rankL, numberL, NL, EiCompL,
     &      H0_L,S0_L, H1_L,S1_L, Sigma_L,nrchanL,
     &      side_rankR, numberR, NR, EiCompR,
     &      H0_R,S0_R, H1_R,S1_R, Sigma_R,nrchanR,deltaimag)


C *****************************************************************
C Calculates the selfenergies based on method by Ivan Rungger
C Reference:
C Written by Ivan Rungger, October 2007
C Computational Spintronics Group
C Trinity College Dublin
C e-mail: runggeri@tcd.ie
C ********** HISTORY **********************************************
C Original version:	October 2007
C ********** INPUT ************************************************

      use parallel
#ifdef MPI
      use mpi_siesta
#endif
      IMPLICIT NONE
      
      INTEGER Nenerg_div,IVSTART,IVFINISH,NSPIN,iktot,NL,NR,
     &    nrchanL,nrchanR,I,ISPIN,iik,N_IVPOINTS,i1
      CHARACTER(LEN=1) :: side_rankL(NSPIN,iktot), 
     &    side_rankR(NSPIN,iktot)
      INTEGER :: numberL(NSPIN,iktot), 
     &    numberR(NSPIN,iktot),Mynode,Nnodes
      DOUBLE COMPLEX :: EiCompL(Nenerg_div), EiCompR(Nenerg_div),
     &    H0_L(NL,NL,NSPIN), H1_L(NL,NL,NSPIN), S0_L(NL,NL),S1_L(NL,NL),
     &    Sigma_L(NL,NL,Nenerg_div,NSPIN,iktot),
     &    H0_R(NR,NR,NSPIN), H1_R(NR,NR,NSPIN), S0_R(NR,NR),S1_R(NR,NR),
     &    Sigma_R(NR,NR,Nenerg_div,NSPIN,iktot),zv(NL)
      DOUBLE PRECISION deltaene,deltaimag
#ifdef MPI
      INTEGER :: MPIerror
#endif

#ifdef MPI
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,mynode,MPIerror)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,Nnodes,MPIerror)
#else
      Nnodes=1
      MyNode=0
#endif

        CALL TIMER('RE_SENE',1)         

!        deltaimag=2d-5

        DO I=IVSTART,IVFINISH

          DO ISPIN=1,NSPIN
!            CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
!            write(*,*)"iselfel",iik
!            call f77flush
!      if(Mynode.eq.3)then
!        open(11111,FILE="info_m2s",RECL=1000000,POSITION='APPEND')
!        write(11111,*)"enespin=",EiCompL(I),ISPIN,iik
!        close(11111)
!      endif

            if(I.lt.IVFINISH)then
              deltaene=ABS(EiCompL(I)-EiCompL(I+1))
            else
              deltaene=ABS(EiCompL(I)-EiCompL(I-1))
            endif
            CALL SELFENERGY2('L',NL,EiCompL(I),H0_L(:,:,ISPIN),
     &          H1_L(:,:,ISPIN),S0_L(:,:),S1_L(:,:),
     &          Sigma_L(:,:,I-N_IVPOINTS,ISPIN,iik),nrchanL,iik,
     &          deltaene,deltaimag)

            if(.false.)then
              call geigenvalues1( Sigma_L(:,:,I-N_IVPOINTS,ISPIN,iik),
     &            zv, NL)

               do i1=1, NL
                 if(ISPIN.eq.1)then
                   write(12347,*)"isil1=",iik,DREAL(EiCompL(I)),
     &               DREAL(zv(i1)),DIMAG(zv(i1))
                 else
                   write(12347,*)"isil2=",iik,DREAL(EiCompL(I)),
     &               DREAL(zv(i1)),DIMAG(zv(i1))
                 endif
               enddo


C              CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
              call writemat8(DREAL(EiCompL(I)),
     &            sigma_l(:,:,I-N_IVPOINTS,ISPIN,iik),nl,nl,-1D0,
     &            "sigmalsml_o")
!              call f77flush
            endif

              if(I.lt.IVFINISH)then
                deltaene=ABS(EiCompR(I)-EiCompR(I+1))
              else
                deltaene=ABS(EiCompR(I)-EiCompR(I-1))
              endif
              CALL SELFENERGY2('R',NR,EiCompR(I),H0_R(:,:,ISPIN),
     &          H1_R(:,:,ISPIN),S0_R(:,:),S1_R(:,:),
     &          Sigma_R(:,:,I+N_IVPOINTS,ISPIN,iik),nrchanR,iik,
     &          deltaene,deltaimag)

            if(.false.)then

              call geigenvalues1( Sigma_R(:,:,I+N_IVPOINTS,ISPIN,iik),
     &            zv, NR)

              do i1=1, NR
                if(ISPIN.eq.1)then
                  write(12347,*)"isir1=",iik,DREAL(EiCompR(I)),
     &              DREAL(zv(i1)),DIMAG(zv(i1))
                else
                  write(12347,*)"isir2=",iik,DREAL(EiCompR(I)),
     &              DREAL(zv(i1)),DIMAG(zv(i1))
                endif
              enddo



C              CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
              call writemat8(DREAL(EiCompR(I)),
     &            sigma_r(:,:,I+N_IVPOINTS,ISPIN,iik),nr,nr,-1D0,
     &            "sigmarsml_o")
!              call f77flush
C              CALL MPI_BARRIER( MPI_COMM_WORLD, MPIerror )
            endif
       
          ENDDO



        ENDDO

        CALL TIMER('RE_SENE',2)


      end SUBROUTINE get_all_selfenergies2
