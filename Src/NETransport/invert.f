! 
! Copyright (c) Smeagol Authors: 
! A. R. Rocha, V. Garcia-Suarez, S. Bailey, C. J. Lambert, J. Ferrer and
! S. Sanvito 2003-2005
! 
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
! "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
! LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
! A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
! OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
! SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
! LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
! DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
! THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
! (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
! OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
!
! SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE (www.smeagol.tcd.ie)
! UPON COMPLETION OF THE "SMEAGOL ACADEMIC LICENSE" .
!
! FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL: smeagol@tcd.ie
!
	SUBROUTINE INVERT(N1,NR,NL,Gr)

C *****************************************************************
C Calculates the inversion of a symmetric complex matrix.
C The matrix is split in nine blocks corresponding to the leads, the
C scattering region and the connections between them
C Only the elements of the columns between NL+1 and N1-NR are calculated,
C i. e., the block-diagonal part corresponding to the left and right leads
C are not calculated
C
C Written by Alexandre Reily Rocha, June 2003 
C Computational Spintronics Group
C Trinity College Dublin
C e-mail: rochaa@tcd.ie
C ***************************** HISTORY ***********************************
C Original version:	January 2005
C ***************************** INPUT *****************************	
C integer N1		: dimension of matrix to be inverted
C integer NL		: dimension of one unit cell of the left lead
C integer NR		: dimension of one unit cell of the right lead
C double complex Gr(N1,N1) : Matrix to be inverted
C **************************** Output ******************************
C double complex Gr(N1,N1) : Inverted matrix
C ******************************************************************
	IMPLICIT NONE
	
	INTEGER :: N1,NL,NR
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: Gr
	DOUBLE COMPLEX, DIMENSION (NL,NL) :: A1
	DOUBLE COMPLEX, DIMENSION (NR,NR) :: A2
	DOUBLE COMPLEX, DIMENSION (NL,N1-NL-NR) :: V1
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,NL) :: V1_dag,aux1
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,NR) :: V2,aux2
	DOUBLE COMPLEX, DIMENSION (NR,N1-NL-NR) :: V2_dag
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,N1-NL-NR) :: B

       INTEGER, PARAMETER :: NB=64
	INTEGER, DIMENSION (N1) :: IPIV
	DOUBLE COMPLEX, DIMENSION (N1*NB) :: WORK
	INTEGER :: INFO,I,J
	
	
	A1=Gr(1:NL,1:NL)
	V1=Gr(1:NL,NL+1:N1-NR)
	V1_dag=Gr(NL+1:N1-NR,1:NL)
	
	A2=Gr(N1-NR+1:N1,N1-NR+1:N1)
	V2=Gr(NL+1:N1-NR,N1-NR+1:N1)
	V2_dag=Gr(N1-NR+1:N1,NL+1:N1-NR)
	
	B=Gr(NL+1:N1-NR,NL+1:N1-NR)
	
	CALL ZSYTRF('U',NL,A1,NL,IPIV(1:NL),WORK(1:NL*NB),NL*NB,INFO)
	CALL ZSYTRI('U',NL,A1,NL,IPIV(1:NL),WORK(1:2*NL),INFO)	 
	
	CALL ZSYMM('R','U',N1-NL-NR,NL,(-1.D0,0.D0),A1,NL,
     &       V1_dag,N1-NL-NR,(0.D0,0.D0),aux1,N1-NL-NR)
        CALL ZGEMM('N','N',N1-NL-NR,N1-NL-NR,NL,(1.D0,0.D0),
     &       aux1,N1-NL-NR,V1,NL,(1.D0,0.D0),B,N1-NL-NR)


	CALL ZSYTRF('U',NR,A2,NR,IPIV(1:NR),WORK(1:NR*NB),NR*NB,INFO)
	CALL ZSYTRI('U',NR,A2,NR,IPIV(1:NR),WORK(1:2*NR),INFO)	 
	    	         
	CALL ZSYMM('R','U',N1-NL-NR,NR,(-1.D0,0.D0),A2,NR,
     &       V2,N1-NL-NR,(0.D0,0.D0),aux2,N1-NL-NR)
        CALL ZGEMM('N','N',N1-NL-NR,N1-NL-NR,NR,(1.D0,0.D0),
     &       aux2,N1-NL-NR,V2_dag,NR,(1.D0,0.D0),B,N1-NL-NR) 
     
	CALL ZSYTRF('U',N1-NL-NR,B,N1-NL-NR,IPIV(1:N1-NL-NR),
     &       WORK,N1*NB,INFO)
	CALL ZSYTRI('U',N1-NL-NR,B,N1-NL-NR,IPIV(1:N1-NL-NR),
     &	     WORK(1:2*(N1-NL-NR)),INFO)	 
	    	         
	CALL ZSYMM('L','U',N1-NL-NR,NL,(1.D0,0.D0),B,N1-NL-NR,
     &       aux1,N1-NL-NR,(0.D0,0.D0),V1_dag,N1-NL-NR)
        CALL ZSYMM('L','U',N1-NL-NR,NR,(1.D0,0.D0),B,N1-NL-NR,
     &       aux2,N1-NL-NR,(0.D0,0.D0),V2,N1-NL-NR)
     
	Gr=0.D0
	DO I=1,N1-NL-NR
	 DO J=I,N1-NL-NR
	  Gr(NL+I,NL+J)=B(I,J)
	  Gr(NL+J,NL+I)=B(I,J)
	 ENDDO
	ENDDO
	
	Gr(NL+1:N1-NR,1:NL)=V1_dag
	Gr(NL+1:N1-NR,N1-NR+1:N1)=V2

	DO J=1,N1-NL-NR
	 DO I=1,NL
	  Gr(I,NL+J)=V1_dag(J,I)
	 ENDDO
	 DO I=1,NR
	  Gr(N1-NR+I,NL+J)=V2(J,I)
	 ENDDO
	ENDDO

	END SUBROUTINE INVERT


	SUBROUTINE INVERTIV(N1,NR,NL,Gr)
	
	IMPLICIT NONE
	
	INTEGER :: N1,NL,NR
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: Gr
	DOUBLE COMPLEX, DIMENSION (NL,NL) :: A1
	DOUBLE COMPLEX, DIMENSION (NR,NR) :: A2
	DOUBLE COMPLEX, DIMENSION (NL,N1-NL-NR) :: V1,aux1_dag
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,NL) :: V1_dag,aux1
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,NR) :: V2,aux2
	DOUBLE COMPLEX, DIMENSION (NR,N1-NL-NR) :: V2_dag,aux2_dag
	DOUBLE COMPLEX, DIMENSION (N1-NL-NR,N1-NL-NR) :: B

       INTEGER, PARAMETER :: NB=64
	INTEGER, DIMENSION (N1) :: IPIV
	DOUBLE COMPLEX, DIMENSION (N1*NB) :: WORK
	INTEGER :: INFO,I,J
	
	A1=Gr(1:NL,1:NL)
	V1=Gr(1:NL,NL+1:N1-NR)
	V1_dag=Gr(NL+1:N1-NR,1:NL)
	
	A2=Gr(N1-NR+1:N1,N1-NR+1:N1)
	V2=Gr(NL+1:N1-NR,N1-NR+1:N1)
	V2_dag=Gr(N1-NR+1:N1,NL+1:N1-NR)
	
	B=Gr(NL+1:N1-NR,NL+1:N1-NR)

	Gr=0.d0
	
	CALL ZSYTRF('U',NL,A1,NL,IPIV(1:NL),WORK(1:NL*NB),NL*NB,INFO)
	CALL ZSYTRI('U',NL,A1,NL,IPIV(1:NL),WORK(1:2*NL),INFO)	 
	
	CALL ZSYMM('R','U',N1-NL-NR,NL,(-1.D0,0.D0),A1,NL,
     &       V1_dag,N1-NL-NR,(0.D0,0.D0),aux1,N1-NL-NR)
        CALL ZGEMM('N','N',N1-NL-NR,N1-NL-NR,NL,(1.D0,0.D0),
     &       aux1,N1-NL-NR,V1,NL,(1.D0,0.D0),B,N1-NL-NR)

        CALL ZSYMM('L','U',NL,N1-NL-NR,(-1.D0,0.D0),A1,NL,
     &       V1,NL,(0.D0,0.D0),aux1_dag,NL)


	CALL ZSYTRF('U',NR,A2,NR,IPIV(1:NR),WORK(1:NR*NB),NR*NB,INFO)
	CALL ZSYTRI('U',NR,A2,NR,IPIV(1:NR),WORK(1:2*NR),INFO)	 
	    	         
	CALL ZSYMM('R','U',N1-NL-NR,NR,(-1.D0,0.D0),A2,NR,
     &       V2,N1-NL-NR,(0.D0,0.D0),aux2,N1-NL-NR)
        CALL ZGEMM('N','N',N1-NL-NR,N1-NL-NR,NR,(1.D0,0.D0),
     &       aux2,N1-NL-NR,V2_dag,NR,(1.D0,0.D0),B,N1-NL-NR) 
    
        CALL ZSYMM('L','U',NR,N1-NL-NR,(-1.D0,0.D0),A2,NR,
     &       V2_dag,NR,(0.D0,0.D0),aux2_dag,NR)
 
	CALL ZSYTRF('U',N1-NL-NR,B,N1-NL-NR,IPIV(1:N1-NL-NR),
     &       WORK,N1*NB,INFO)
	CALL ZSYTRI('U',N1-NL-NR,B,N1-NL-NR,IPIV(1:N1-NL-NR),
     &	     WORK(1:2*(N1-NL-NR)),INFO)	 
	    	         
	CALL ZSYMM('L','U',N1-NL-NR,NL,(1.D0,0.D0),B,N1-NL-NR,
     &       aux1,N1-NL-NR,(0.D0,0.D0),Gr(NL+1:N1-NR,1:NL),N1-NL-NR)
        CALL ZSYMM('L','U',N1-NL-NR,NR,(1.D0,0.D0),B,N1-NL-NR,
     &       aux2,N1-NL-NR,(0.D0,0.D0),Gr(NL+1:N1-NR,N1-NR+1:N1),
     &       N1-NL-NR)

	CALL ZGEMM('N','N',NR,NL,N1-NR-NL,(1.0d0,0.d0),aux2_dag,NR,
     &       Gr(NL+1:N1-NR,1:NL),N1-NL-NR,(0.d0,0.d0),
     &       Gr(N1-NL+1:N1,1:NL),NR)

        CALL ZGEMM('N','N',NL,NL,N1-NR-NL,(1.0d0,0.d0),aux1_dag,NL,
     &       Gr(NL+1:N1-NR,1:NL),N1-NL-NR,(1.d0,0.d0),a1,NL)

        CALL ZGEMM('N','N',NR,NR,N1-NR-NL,(1.0d0,0.d0),aux2_dag,NR,
     &       Gr(NL+1:N1-NR,N1-NR+1:N1),N1-NL-NR,(1.d0,0.d0),a2,NR)

 	DO I=1,NL    
	 DO J=1,N1-NL
	  Gr(I,NL+J)=Gr(NL+J,I)
	 ENDDO
	 DO J=I,NL
	  Gr(I,J)=a1(I,J)
	  Gr(J,I)=a1(I,J)
	 ENDDO
	ENDDO

	DO I=1,NR
	 DO J=1,N1-NL-NR
	  Gr(N1-NR+I,NL+J)=Gr(NL+J,N1-NR+I)
	 ENDDO
	 DO J=I,NR
	  Gr(N1-NR+J,N1-NR+I)=a2(I,J)
	  Gr(N1-NR+I,N1-NR+J)=a2(I,J)
	 ENDDO
	ENDDO

	END SUBROUTINE INVERTIV
