      SUBROUTINE simpson(x1,x2,x,w,n)

      implicit none

      INTEGER n
      DOUBLE PRECISION x1,x2,x(n),w(n)
      INTEGER i,j,m
      DOUBLE PRECISION dx

      dx=(x2-x1)/REAL(n-1)

      DO i=1,n
        x(i)=x1+(i-1)*dx
        IF (MOD(n,2).NE.0) THEN
         IF((i.EQ.1).OR.(i.eq. n)) then
           w(i)=1.0d0/3.0d0*dx
         ELSEIF (MOD(i,2).EQ.0) THEN
           w(I)=4.0d0/3.0d0*dx
         ELSE
           w(I)=2.0d0/3.0d0*dx
         ENDIF
        ELSE
C Tomar cuidado, testar se NTenerg_div eh igual a 1
         IF(i.EQ.1) THEN
           w(I)=1.0d0/3.0d0*dx
         ELSEIF (i.eq. n-1) then
           w(I)=(1.0d0/3.0d0+1.0d0/2.0d0)*dx
         ELSEIF (i.eq.n) then
           w(I)=1.0d0/2.0d0*dx
         ELSEIF (MOD(i,2).EQ.0) THEN
           w(I)=4.0d0/3.0d0*dx
         ELSE
           w(I)=2.0d0/3.0d0*dx
         ENDIF
        ENDIF
       ENDDO

      write(6,*) "smeagol: Simpson "
      write(6,*) "smeagol: energy spacing: ",dx
      write(6,*) "smeagol: sum of weights: ",sum(w)/(x2-x1)

      END
