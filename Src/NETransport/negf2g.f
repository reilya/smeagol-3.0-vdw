!  *******************************************************************  !
!  Copyright (c) Smeagol Authors (2003-2005):  A. R. Rocha,             !
!                                              V. Garcia-Suarez,        !
!                                              S. Bailey,               !
!                                              C. J. Lambert,           !
!                                              J. Ferrer and            !
!                                              S. Sanvito               !
!                                                                       !
!  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS  !
!  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT    !
!  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS    !
!  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE      !
!  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,  !
!  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES             !
!  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR   !
!  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)   !
!  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,  !
!  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)        !
!  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  !
!  OF THE POSSIBILITY OF SUCH DAMAGE.                                   !
!                                                                       !
!  SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE              !
!  (www.smeagol.tcd.ie) UPON COMPLETION OF "SMEAGOL ACADEMIC LICENSE".  !
!                                                                       !
!  FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL smeagol@tcd.ie  !
!  *******************************************************************  !

      subroutine negf2g (nuo2, nsc, istep, ivv, iscf, VBias,
     .                   Haux, Saux, last, rhoG)
!  *******************************************************************  !
!                                negf2g                                 !
!  *******************************************************************  !
!  Description: calculates the charge density of a nanoscopic system    !
!  coupled to charge reservoirs. It uses the non-equilibrium Green's    !
!  function approach. The equilibrium charge density is calculated      !
!  using a contour integral in the complex plane.                       !
!                                                                       !
!  Collinear spins, k points version.                                   !
!                                                                       !
!  On convergence the current is calculated (and possibly the           !
!  transmission coefficients).                                          !
!                                                                       !
!  Based on subroutine negf.F .                                         !
!                                                                       !
!  Written by Victor Garcia-Suarez, November 2003.                      !
!  Departamento de Fisica                                               !
!  Universidad de Oviedo                                                !
!  e-mail: victor@condmat.uniovi.es                                     !
!  ***************************** HISTORY *****************************  !
!  Original version:    November 2003                                   !
!  Modified version:    November 2012 (transitioned to SIESTA 3.1 and   !
!                       voltages at '.CUR' file written in eV, by       !
!                       Pedro Brandimarte Universidade de Sao Paulo,    !
!                       e-mail: brandimarte@gmail.com)                  !
!  ****************************** INPUT ******************************  !
!  integer nuo2	           : Dimension of the basis orbitals            !
!  integer nsc(2)          : Number of unit cells along parallel        !
!                            directions                                 !
!  integer istep           : Molecular dynamics step                    !
!  integer ivv             : Voltage step                               !
!  integer iscf            : SCF iteration number                       !
!  real*8 VBias            : Bias potential                             !
!  logical last            : If last SCF iteration (DM has converged?)  !
!  ************************ INPUT (AUXILIARY) ************************  !
!  complex*16 Haux(nuo2,nuo2) : Hamiltonian matrix                      !
!  complex*16 Saux(nuo2,nuo2) : Overlap matrix                          !
!  *********************** INPUT FROM MODULES ************************  !
!  real*8 temp             : Electronic temperature in Ry               !
!  integer NEnergR         : Number of energy points for real axis      !
!                            integration                                !
!  integer NEnergIC        : Number of energy points of semi-circular   !
!                            region for contour integral                !
!  integer NEnergIL        : Number of energy points of linear region   !
!                            for contour integral                       !
!  integer NPoles          : Number of poles in the Fermi-Dirac distr.  !
!  real*8 imDelta          : Small imaginary part in the energy         !
!  real*8 EnergLB          : Energy below lowest band                   !
!  real*8 VInitial         : Initial value of the bias potential        !
!  real*8 VFinal           : Final value of the bias potential          !
!  integer SpinCL          : True = Leads with different spin           !
!  integer NSlices         : Number of unit cells to be substituted     !
!                            by bulk values                             !
!  logical TrCoeff         : Toggles calculation of the transmission    !
!  integer inicoor         : Initial step in geommetry iteration        !
!  character*20 slabel     : System label                               !
!  ***************************** OUTPUT ******************************  !
!  complex*16 rhoG(nuo2,nuo2) : The charge density matrix               !
!  **************************** CONSTANTS ****************************  !
!  real*8 PI               : Number Pi=3.1415...                        !
!  real*8 e                : Electron charge = 1.0DD0                   !
!  real*8 kB               : The Boltzmann constant in units of Ry/K    !
!  complex*16 zi           : The imaginary unit = 0+1i                  !
!  ************************* SAVED VARIABLES *************************  !
!  integer NL              : Dimensions of the basis orbitals in left   !
!                            lead                                       !
!  integer NR              : Dimensions of the basis orbitals in right  !
!                            lead                                       !
!  integer IVSTART         : Starting index of real axis integration    !
!  integer IVFINISH        : Finishing index of real axis integration   !
!  integer N_IVPOINTS      :                                            !
!  logical periodic        :                                            !
!  complex*16 H0_L(NL,NL)  : Hamiltonian left lead                      !
!  complex*16 H1_L(NL,NL)  : Coupling matrix left lead                  !
!  complex*16 S0_L(NL,NL)  : On-site Overlap matrix left lead           !
!  complex*16 S1_L(NL,NL)  : Neighbour Overlap matrix left lead         !
!  complex*16 H0_R(NR,NR)  : Hamiltonian right lead                     !
!  complex*16 H1_R(NR,NR)  : Coupling matrix right lead                 !
!  complex*16 S0_R(NR,NR)  : On-site Overlap matrix right lead          !
!  complex*16 S1_R(NR,NR)  : Neighbour Overlap matrix right lead        !
!  complex*16 Sigma_L(NL,NL,NEnergR) : Self-energies left lead real     !
!                                      axis                             !
!  complex*16 Sigma_R(NR,NR,NEnergR) : Self-energies right lead real    !
!                                      axis                             !
!  complex*16 ZSigma_L(NL,NL,NEnergIL+NEnergIC+NPoles) : Self energies  !
!                                      left lead complex plane          !
!  complex*16 ZSigma_R(NR,NR,NEnergIL+NEnergIC+NPoles) : Self energies  !
!                                      right lead complex plane         !
!  complex*16 QL(NL,NL)    :                                            !
!  complex*16 QR(NR,NR)    :                                            !
!  complex*16 VHL(NL,NL)   :                                            !
!  complex*16 VHR(NR,NR)   :                                            !
!  complex*16 VSL(NL,NL)   :                                            !
!  complex*16 VSR(NR,NR)   :                                            !
!  integer numberL         :                                            !
!  integer numberR         :                                            !
!  real*8 EnergI           : Initial energy for real axis integration   !
!  real*8 EnergF           : Final energy for real axis integration     !
!  real*8 Ef_Lead          : Fermi energy                               !
!  real*8 Ef_LeadL         :                                            !
!  real*8 Ef_LeadR         :                                            !
!  real*8 dE               :                                            !
!  real*8 R0               :                                            !
!  real*8 Ic(2)            :                                            !
!  complex*16 EContour(NEnergIL+NEnergIC+NPoles) : Gaussian quadrature  !
!                                 points for contour integral           !
!  complex*16 WContour(NEnergIL+NEnergIC+NPoles) : Gaussian quadrature  !
!                                 weights for contour integral          !
!  integer NenergIm_div     :                                           !
!  integer NenergImNode     :                                           !
!  integer Nsmall_div       :                                           !
!  integer Nenerg_div_nodes :                                           !
!  **************************** AUXILIARY ****************************  !
!  complex*16 Deltarho(nuo2,nuo2) : DM out of equilibrium               !
!  complex*16 rho0(nuo2,nuo2)     : equilibrium DM                      !
!  *******************************************************************  !

      use precision,       only: dp
      use siesta_options,  only: temp
      use smeagol_options, only: NEnergR, NEnergIC, NEnergIL, NPoles,
     .                           imDelta, EnergLB, VInitial, VFinal,
     .                           SpinCL, NSlices, TrCoeff
      use m_steps,         only: inicoor
      use files,           only: slabel
      use fdf
      use chargebias,      only: chargebias_EQ
       
! "negf2g_include.h" BEGIN

      implicit none

      INCLUDE "const.h"


!     *** Input variables ***
      integer :: nuo2, nsc(2), istep, ivv, iscf
      real(dp) :: VBias
      real(dp) :: rhoG(nuo2,nuo2)
      complex(dp) :: Haux(nuo2,nuo2), Saux(nuo2,nuo2)
      logical :: last

!     *** Internal variables ***

!     Parameters.
      integer :: nspinL, maxnhL, nspinR, maxnhR
      integer, save :: N12, NL, NL2, NR, NR2,
     .                 N_IVPOINTS, IVSTART, IVFINISH,
     .                 NenergIm_div, NenergImNode
      logical, save :: periodic = .TRUE.

!     Labels.
      character slabelL*20, slabelR*20, paste*25, curfile*25, lgfile*25
        
!     Constant arrays.
      complex(dp), allocatable, save :: Sigma_L(:,:,:), Sigma_R(:,:,:),
     .                                  ZSigma_L(:,:,:), ZSigma_R(:,:,:)

!     Charge density.
      complex(dp), allocatable :: Deltarho(:,:), rho0(:,:)

!     Leads H_0, H_1, S_0 and S_1.
      complex(dp), allocatable, save :: H0_L(:,:), H1_L(:,:),
     .                                  S0_L(:,:), S1_L(:,:),
     .                                  H0_R(:,:), H1_R(:,:),
     .                                  S0_R(:,:), S1_R(:,:)

!     Decimation variables.
      integer, save :: numberL(1), numberR(1)
      complex(dp), allocatable, save :: QR(:,:), QL(:,:),
     .                                  VHL(:,:), VHR(:,:),
     .                                  VSL(:,:), VSR(:,:)

!     Fermi-Dirac distributions for leads L and R.
      complex(dp) :: CONST, fL, fR

!     Energy Related Variables.
      real(dp) :: Ei, Ic
      real(dp) :: kpoint(3)
      real(dp), save :: EnergI, EnergF, Ef_Lead, dE, R0,
     .                  Ef_LeadL, Ef_LeadR
      complex(dp), allocatable, save :: EContour(:), WContour(:)

!     Auxiliaries.
      integer :: I, J, II, JJ, L, K, iu, iuc, iul, nrchan,
     .           ISPIN_CASE, IE_CASE, INFO
      integer,  allocatable :: IPIV(:)
      real(dp) :: fermi_aux
      complex(dp), allocatable :: Tau1_aux(:,:),
     .                            GF1_aux(:,:), GF2_aux(:,:),
     .                            GF_iter(:,:), GF_iter_dag(:,:),
     .                            Sigma_aux(:,:), aux(:,:), Ic_aux(:),
     .                            WORK(:),
     .                            Gamma_L(:,:), Gamma_R(:,:),
     .                            Gamma1_aux(:,:), Gamma2_aux(:,:)
        
      logical :: fileexist, leadsgf

! "negf2g_include.h" END

      character(LEN=1), save :: side_rankL, side_rankR
      complex(dp) :: EiComp
        
      CALL TIMER('NEGF2G',1)

      If (SpinCL .EQ. 1) Then
         periodic=.FALSE.
      EndIf
         
      IF ((iscf.EQ.1) .AND. (istep.EQ.inicoor) .AND. (ivv.EQ.0)) THEN

!        Read some data.
         call io_assign(iu)
         open(iu,file='bulklft.DAT',status='old')
         read(iu,*) slabelL, NL2, nspinL, maxnhL, Ef_LeadL
         call io_close(iu)

         call io_assign(iu)
         open(iu,file='bulkrgt.DAT',status='old')
         read(iu,*) slabelR, NR2, nspinR, maxnhR, Ef_LeadR
         call io_close(iu)

         if (abs(Ef_LeadL-Ef_LeadR).GT.1.d-5) then
            write(6,'(a50)') 
     .           'SMEAGOL: Smeagol cannot perform calculations with'
            write(6,'(a39)') 'different leads and noncollinear spins'
            stop
         endif

         N12 = nuo2/2
         NL = NL2*2
         NR = NR2*2
         Ef_Lead = Ef_LeadL

!        Allocaction of memory.
         allocate(H0_L(NL,NL), H1_L(NL,NL))
         call memory('A','Z',4*NL*NL,'negf2g')
         allocate(S0_L(NL,NL), S1_L(NL,NL))
         call memory('A','Z',4*NL*NL,'negf2g')
         allocate(H0_R(NR,NR), H1_R(NR,NR))
         call memory('A','Z',4*NR*NR,'negf2g')
         allocate(S0_R(NR,NR), S1_R(NR,NR))
         call memory('A','Z',4*NR*NR,'negf2g')

!        Read and calculate the Hamiltonians, overlaps and density
!        matrices of the leads.
         kpoint = 0.d0
         call hsleads(4, 1, NL, NR, nsc, iscf, istep, ivv,
     .        .true., 1, kpoint, temp,
     .        H0_L, H1_L, S0_L, S1_L, H0_R, H1_R, S0_R, S1_R)

         ALLOCATE(QL(NL,NL),VHL(NL,NL),VSL(NL,NL))
         ALLOCATE(QR(NR,NR),VHR(NR,NR),VSR(NR,NR))
     

         Call RANK(NL,S1_L,H1_L,side_rankL)
         If (side_rankL .NE. '0') Then
            Call GENSVD(NL,S1_L,H1_L,side_rankL,
     &           numberL,VHL,VSL,QL)
         Else
            numberL=0
            VHL=H1_L
            VSL=S1_L
            QL=0.0
            Do I=1,NL
               QL(I,I)=1.D0
            EndDo
         EndIf
           
         Call RANK(NR,S1_R,H1_R,side_rankR)
         If (side_rankR .NE. '0') Then
            Call GENSVD(NR,S1_R,H1_R,side_rankR,
     &           numberR,VHR,VSR,QR)
         Else
            numberR=0.D0         
            VHR=H1_R
            VSR=S1_R
            QR=0.0
            Do I=1,NR
               QR(I,I)=1.D0
            EndDo
         EndIf
          
      ENDIF

      DO I=1,NSlices
         DO j = 1,2
            DO k = 1,2
               Haux((j-1)*N12+(I-1)*NL2+1:(j-1)*N12+I*NL2,
     &              (k-1)*N12+(I-1)*NL2+1:(k-1)*N12+I*NL2)=
     &              H0_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)+
     &              VBias/2.D0*S0_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)
               Saux((j-1)*N12+(I-1)*NL2+1:(j-1)*N12+I*NL2,
     &              (k-1)*N12+(I-1)*NL2+1:(k-1)*N12+I*NL2) =
     &              S0_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)

               Haux(j*N12-I*NR2+1:j*N12-(I-1)*NR2,
     &              k*N12-I*NR2+1:k*N12-(I-1)*NR2)=
     &              H0_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)-
     &              VBias/2.D0*S0_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)
               Saux(j*N12-I*NR2+1:j*N12-(I-1)*NR2,
     &              k*N12-I*NR2+1:k*N12-(I-1)*NR2)=
     &              S0_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)
            ENDDO
         ENDDO

         IF (I .GE. 2) THEN
            DO j = 1,2
               DO k = 1,2
                  Haux((j-1)*N12+(I-2)*NL2+1:(j-1)*N12+(I-1)*NL2,
     &                 (k-1)*N12+(I-1)*NL2+1:(k-1)*N12+I*NL2)=
     &                 H1_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)+VBias/
     &                 2.D0*S1_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)

                  Saux((j-1)*N12+(I-2)*NL2+1:(j-1)*N12+(I-1)*NL2,
     &                 (k-1)*N12+(I-1)*NL2+1:(k-1)*N12+I*NL2)=
     &                 S1_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2)

                  Haux(j*N12-I*NR2+1:j*N12-(I-1)*NR2,
     &                 k*N12-(I-1)*NR2+1:k*N12-(I-2)*NR2)=
     &                 H1_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)-VBias/
     &                 2.D0*S1_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)

                  Saux(j*N12-I*NR2+1:j*N12-(I-1)*NR2,
     &                 k*N12-(I-1)*NR2+1:k*N12-(I-2)*NR2)=
     &                 S1_R((j-1)*NR2+1:j*NR2,(k-1)*NR2+1:k*NR2)
               ENDDO
            ENDDO

            DO II=1,MAX(NL2,NR2)
               DO JJ=1,MAX(NL2,NR2)
                  IF ((II .LE. NL2) .AND. (JJ .LE. NL2)) THEN
                     DO j = 1,2
                        DO k = 1,2
                           Saux((j-1)*N12+(I-1)*NL2+II,
     &                          (k-1)*N12+(I-2)*NL2+JJ)=
     &                          DCONJG(S1_L((k-1)*NL2+JJ,(j-1)*NL2+II))
                        ENDDO
                     ENDDO
                  ENDIF

                  IF ((II .LE. NR2) .AND. (JJ .LE. NR2)) THEN 
                     DO j = 1,2
                        DO k = 1,2
                           Saux(j*N12-(I-1)*NR2+II,k*N12-I*NR2+JJ)=
     &                          DCONJG(S1_R((k-1)*NR2+JJ,(j-1)*NR2+II))
                        ENDDO
                     ENDDO
                  ENDIF

                  IF ((II .LE. NL2) .AND. (JJ .LE. NL2)) THEN  
                     DO j = 1,2
                        DO k = 1,2
                           Haux((j-1)*N12+(I-1)*NL2+II,
     &                          (k-1)*N12+(I-2)*NL2+JJ)=
     &                          DCONJG(H1_L((k-1)*NL2+JJ,(j-1)*NL2+II))+
     &                          VBias/2.D0*
     &                          DCONJG(S1_L((k-1)*NL2+JJ,(j-1)*NL2+II))
                        ENDDO
                     ENDDO
                  ENDIF

                  IF ((II .LE. NR2) .AND. (JJ .LE. NR2)) THEN 
                     DO j = 1,2
                        DO k = 1,2
                           Haux(j*N12-(I-1)*NR2+II,k*N12-I*NR2+JJ)=
     &                          DCONJG(H1_R((k-1)*NR2+JJ,(j-1)*NR2+II))-
     &                          VBias/2.D0*
     &                          DCONJG(S1_R((k-1)*NR2+JJ,(j-1)*NR2+II))
                        ENDDO
                     ENDDO
                  ENDIF
               ENDDO
            ENDDO
         ENDIF
      ENDDO

      IF ((NL2 .NE. N12) .AND. periodic) THEN 
         DO j = 1,2
            DO k = 1,2
               Haux((j-1)*N12+1:(j-1)*N12+NL2,k*N12-NL2+1:k*N12)=0.D0
               Saux((j-1)*N12+1:(j-1)*N12+NL2,k*N12-NL2+1:k*N12)=0.D0
            ENDDO
         ENDDO
      ENDIF

      IF ((NR2 .NE. N12) .AND. periodic) THEN
         DO j = 1,2
            DO k = 1,2
               Haux(j*N12-NR2+1:j*N12,(k-1)*N12+1:(k-1)*N12+NR2)=0.D0
               Saux(j*N12-NR2+1:j*N12,(k-1)*N12+1:(k-1)*N12+NR2)=0.D0
            ENDDO
         ENDDO
      ENDIF

      IF ((ivv.EQ.0) .AND. (istep.EQ.inicoor) .AND. (iscf.EQ.1)) THEN
         ALLOCATE(Sigma_L(NL,NL,NEnergR),Sigma_R(NR,NR,NEnergR))
         CALL MEMORY('A','Z',NL*NL*NEnergR,'NEGF2G')
         CALL MEMORY('A','Z',NR*NR*NEnergR,'NEGF2G')
         
         EnergI=Ef_Lead-MAX(DABS(VInitial),DABS(VFinal))-16.D0*kB*temp
         EnergF=Ef_Lead+MAX(DABS(VInitial),DABS(VFinal))+16.D0*kB*temp
         dE=(EnergF-EnergI)/(NEnergR-1)
      ENDIF

      IF (iscf .EQ. 1) THEN
         N_IVPOINTS=0
         IF (DABS(VBias/2.D0) .GT. 1.D-7) THEN
            DO WHILE (N_IVPOINTS*dE .LE. DABS(VBias/2.D0))
               N_IVPOINTS=N_IVPOINTS+1
            ENDDO
            N_IVPOINTS=N_IVPOINTS-1 
            IF (VBias .GT. 0.D0) THEN
               VBias=2.D0*N_IVPOINTS*dE
            ELSE
               N_IVPOINTS=-1*N_IVPOINTS
               VBias=2.D0*N_IVPOINTS*dE
            ENDIF

            IVSTART=0
            IVFINISH=0
            DO I=1,NEnergR
               IF ((IVSTART .EQ. 0) .AND. (EnergI+(I-1)*dE 
     &              .GT. Ef_Lead-DABS(VBias/2.D0)-16.0D0*kB*temp)) THEN
                  IVSTART=I
               ENDIF
               IF ((IVFINISH .EQ. 0) .AND. (EnergI+(I-1)*dE 
     &              .GT. Ef_Lead+DABS(VBias/2.D0)+16.0D0*kB*temp)) THEN
                  IVFINISH=I
               ENDIF
            ENDDO
         ELSE
            IVSTART=0
            IVFINISH=-1
         ENDIF
      ENDIF

      IF ((ivv.EQ.0) .AND. (istep.EQ.inicoor) .AND. (iscf.EQ.1)) THEN
! #####################################################################
! We check the existence of the file: leads_green_function.in, containing
! the self-energies of the leads to which the system is coupled

         lgfile = paste(slabel,'.LGF')
         INQUIRE(FILE=lgfile,EXIST=fileexist)
         call io_assign(iul)
         leadsgf = fdf_boolean('UseLeadsGF',.true.)
         CALL TIMER('RE_SENE',1)

!        If the file containing the matrices for the leads
!        already exists we read this file.
         IF (fileexist.and.leadsgf) THEN
            OPEN(UNIT=iul,FILE=lgfile,STATUS='OLD', FORM='unformatted')
            READ(iul) ISPIN_CASE
            IF (ISPIN_CASE .NE. SpinCL) THEN
               PRINT*,
     &              "The Spins from the self energy input file does" //
     &              " not match"
               PRINT*, "the spin configuration in the program"
               STOP
            ENDIF

            READ(iul) IE_CASE
            IF (IE_CASE .NE. NEnergR) THEN
               PRINT*, "The number of energy points in the file for"
               PRINT*, "the self-energies is not equal to the number of"
               PRINT*, "energy points for the problem"
               STOP
            ENDIF

            DO I=1,NEnergR
               READ(iul) Sigma_L(1:NL,1:NL,I)
               READ(iul) Sigma_R(1:NR,1:NR,I)
            ENDDO

!        If it does not exist, we calculate everything from scratch.
         ELSE

            OPEN(UNIT=iul,FILE=lgfile,STATUS='UNKNOWN',
     &           FORM='unformatted')

!           Calculation of the self-energies of the leads.
            WRITE(iul) SpinCL
            WRITE(iul) NEnergR

            Ei=EnergI
            DO I=1,NEnergR
               Ei=EnergI+(I-1)*dE           
               EiComp=DCMPLX(Ei)
               CALL SELFENERGY('L',side_rankL,numberL,NL,
     &              EiComp,H0_L,VHL,S0_L,VSL,
     &              Sigma_L(1:NL,1:NL,I),QL,INFO,nrchan)
               If (info .eq. 0) Then
                  CALL SELFENERGY('R',side_rankR,numberR,NR,
     &                 EiComp,H0_R,VHR,S0_R,VSR,
     &                 Sigma_R(1:NR,1:NR,I),QR,INFO,nrchan)
               EndIf

               If (INFO .EQ. 1) Then
                  Print*,
     &                 'The condition number of the matrix H1 was' //
     &                 ' too large'
                  Print*, 'The Matrix is possibly singular'
                  Print*, EiComp
                  stop
               EndIf

!              Store the leads green function in a file.
               WRITE(iul) Sigma_L(1:NL,1:NL,I)
               WRITE(iul) Sigma_R(1:NR,1:NR,I)
               Ei=Ei+dE
            ENDDO
         ENDIF
         call io_close(iul)
!         PRINT*, 'Time for Leads Green Function = ',dtime(t0)
         CALL TIMER('RE_SENE',2)
      ENDIF

      IF (.NOT. last) THEN
         IF (iscf .EQ. 1) THEN
            NenergIm_div=NEnergIL+NEnergIC+2*NPoles
            NenergImNode=NEnergIL+NEnergIC+2*NPoles
            ALLOCATE(ZSigma_L(NL,NL,NenergIm_div))
            ALLOCATE(ZSigma_R(NR,NR,NenergIm_div))
            ALLOCATE(EContour(NenergIm_div))
            ALLOCATE(WContour(NenergIm_div))

            CALL MEMORY('A','Z',NL*NL*2*NenergIm_div,'NEGF2G')
            CALL MEMORY('A','Z',2*NenergIm_div,'NEGF2G')

            ZSigma_L=0.D0
            ZSigma_R=0.D0
            EContour=0.D0
            WContour=0.D0

            CALL TIMER('IM_SENE',1)
            CALL CHARGEBIAS_EQ(NEnergIL,NEnergIC,NPoles,NL,NR,
     &           NenergIm_div,NenergImNode,1,R0,EnergLB,Ef_Lead,
     &           VBias,temp,H0_L,VHL,S0_L,VSL,H0_R,VHR,S0_R,VSR,
     &           ZSigma_L,ZSigma_R,EContour,WContour,numberL,
     &           side_rankL,QL,numberR,side_rankR,QR)
  
            CALL TIMER('IM_SENE',2) 
         ENDIF


!        *** Allocating Memory ***

!        Auxiliaries.
         ALLOCATE(GF_iter(nuo2,nuo2),GF_iter_dag(nuo2,nuo2))
         ALLOCATE(aux(nuo2,nuo2),Sigma_aux(nuo2,nuo2))
         ALLOCATE(Tau1_aux(nuo2,nuo2))
         ALLOCATE(IPIV(nuo2),WORK(nuo2**2))
         CALL MEMORY('A','Z',5*nuo2*nuo2,'NEGF2G')
         CALL MEMORY('A','I',nuo2,'NEGF2G')
         CALL MEMORY('A','Z',nuo2**2,'NEGF2G')

!        Charge Density Matrix.
         ALLOCATE(Deltarho(nuo2,nuo2),rho0(nuo2,nuo2))
         CALL MEMORY('A','D',2*nuo2*nuo2,'NEGF2G')

!        ******

         rho0=0.D0
         Sigma_aux=(0.D0,0.D0)

!        ### Charge in equilibrium loop ###
         DO I=1,NEnergIL+NEnergIC+NPoles
            IF (I .LE. NEnergIL+NEnergIC) THEN
               IF (DREAL((EContour(I)-Ef_Lead-e*VBias/2.D0)/(kB*temp))
     &              .GT. 40.D0) THEN
                  CONST=0.D0
               ELSE
                  IF (I .LE. NEnergIL) THEN
                     CONST=1.D0/(CDEXP(DCMPLX(DREAL(EContour(I))-
     &                    Ef_Lead-e*VBias/2.D0)/(kB*temp))+1.D0)
                  ELSE
                     CONST=zi*(EContour(I)-DCMPLX(R0))/
     &                    (CDEXP((EContour(I)-
     &                    Ef_Lead-e*VBias/2.D0)/(kB*temp))+1.D0)
                  ENDIF
               ENDIF
            ELSE
               CONST=1.D0
            ENDIF

            DO j = 1,2
               DO k = 1,2
                  Sigma_aux((j-1)*N12+1:(j-1)*N12+NL2,
     &                 (k-1)*N12+1:(k-1)*N12+NL2)=
     &                 ZSigma_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2,I)
               ENDDO
            ENDDO
            IF ((NL2 .EQ. N12) .AND. (NR2 .EQ. N12)) THEN
               DO j = 1,2
                  DO k = 1,2
                     Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                    (k-1)*N12+1:(k-1)*N12+NR2)=
     &                    Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                    (k-1)*N12+1:(k-1)*N12+NR2)+ZSigma_R((j-1)*
     &                    NR2+1:j*NR2,(k-1)*NR2+1:k*NR2,I)
                  ENDDO
               ENDDO
            ELSE
               DO j = 1,2
                  DO k = 1,2
                     Sigma_aux(j*N12-NR2+1:j*N12,k*N12-NR2+1:k*N12)=
     &                    ZSigma_R((j-1)*NR2+1:
     &                    j*NR2,(k-1)*NR2+1:k*NR2,I)
                  ENDDO
               ENDDO
            ENDIF

            GF_iter=EContour(I)*Saux-Haux-Sigma_aux

            CALL ZGETRF(nuo2,nuo2,GF_iter,nuo2,IPIV,INFO)
            CALL ZGETRI(nuo2,GF_iter,nuo2,IPIV,WORK,nuo2**2,INFO)

            DO II=1,N12
               DO JJ=1,N12
                  IF (((II .GT. NL2) .AND. (II .LE. N12-NR2)) .OR.
     &                 ((JJ .GT. NL2) .AND. (JJ .LE. N12-NR2))) THEN
                     DO j = 1,2
                        DO k = 1,2
                           rho0((j-1)*N12+II,(k-1)*N12+JJ)=
     &                          rho0((j-1)*N12+II,(k-1)*N12+JJ)-
     &                          zi/(2*PI)*(DREAL(CONST*WContour(I)*
     &                          (GF_iter((k-1)*N12+II,(j-1)*N12+JJ)-
     &                          GF_iter((j-1)*N12+JJ,(k-1)*N12+II)))+
     &                          zi*DIMAG(CONST*WContour(I)*
     &                          (GF_iter((j-1)*N12+II,(k-1)*N12+JJ)+
     &                          GF_iter((k-1)*N12+JJ,(j-1)*N12+II))))
                        ENDDO
                     ENDDO
                  ENDIF
               ENDDO
            ENDDO

            IF (MOD(iscf-1,10) .EQ. 0) THEN
               IF (I .EQ. 1) THEN
                  WRITE(112,*) '# Iteration',iscf
                  WRITE(112,*)
               ENDIF
               DO j = 1,2
                  DO k = 1,2
                     WRITE(112,'(4f16.9)') DREAL(EContour(I)),
     &                    DIMAG(EContour(I)),
     &                    DIMAG(GF_iter((j-1)*N12+NL2+1,
     &                    (k-1)*N12+NL2+1)),
     &                    DIMAG(GF_iter(j*N12-NR2,k*N12-NR2))
                  ENDDO
               ENDDO
            ENDIF
         ENDDO
!        ######

         Deltarho=0.D0
         Sigma_aux=(0.D0,0.D0)

!        ### Energy Loop out of equilibrium ###
         IF (MOD(iscf-1,10) .EQ. 0) THEN
            WRITE(110,*) 
            WRITE(110,*) '# Iteration',iscf,NL+1,nuo2-NR
         ENDIF
         
         DO I=IVSTART,IVFINISH
            Ei=EnergI+(I-1)*dE
           
            fermi_aux=(Ei-Ef_Lead-e*VBias/2.D0)/(kB*temp)
            IF ( fermi_aux .GT. 50.D0) THEN
               fL=0.D0
            ELSE
               fL=1.D0/(1.D0+DEXP(fermi_aux))
            ENDIF

            fermi_aux=(Ei-Ef_Lead+e*VBias/2.D0)/(kB*temp)
            IF ( fermi_aux .GT. 50.D0) THEN
               fR=0.D0
            ELSE
               fR=1.D0/(1+DEXP(fermi_aux))
            ENDIF

            IF ((I .EQ. IVSTART) .OR. (I .EQ. IVFINISH)) THEN
               CONST=1.0D0/3.0D0
            ELSE
               IF (MOD(I-IVSTART+1,2) .EQ. 0) THEN
                  CONST=4.D0/3.D0
               ELSE
                  CONST=2.D0/3.D0
               ENDIF
            ENDIF

            IF (CDABS(fL-fR) .GT. 1.D-7) THEN 

               DO j = 1,2
                  DO k = 1,2
                     Sigma_aux((j-1)*N12+1:(j-1)*N12+NL2,
     &                    (k-1)*N12+1:(k-1)*N12+NL2)=
     &                    Sigma_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2,
     &                    I-N_IVPOINTS)
                  ENDDO
               ENDDO
               IF ((NL2 .EQ. N12) .AND. (NR2 .EQ. N12)) THEN
                  DO j = 1,2
                     DO k = 1,2
                        Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                       (k-1)*N12+1:(k-1)*N12+NR2)=
     &                       Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                       (k-1)*N12+1:(k-1)*N12+NR2)+
     &                       Sigma_R((j-1)*NR2+1:
     &                       j*NR2,(k-1)*NR2+1:k*NR2,I+N_IVPOINTS)
                     ENDDO
                  ENDDO
               ELSE
                  DO j = 1,2
                     DO k = 1,2
                        Sigma_aux(j*N12-NR2+1:j*N12,k*N12-NR2+1:k*N12)=
     &                       Sigma_R((j-1)*NR2+1:
     &                       j*NR2,(k-1)*NR2+1:k*NR2,I+N_IVPOINTS)
                     ENDDO
                  ENDDO
               ENDIF

! *****************************************************************
! Calculate the Keldysh Green's function.
               GF_iter=(Ei+zi*imDelta)*Saux-Haux-Sigma_aux

               CALL ZGETRF(nuo2,nuo2,GF_iter,nuo2,IPIV,INFO)
               CALL ZGETRI(nuo2,GF_iter,nuo2,IPIV,WORK,nuo2**2,INFO)
! *****************************************************************

! *****************************************************************
! The Complex Conjugate of the Green Function.
               DO II=1,nuo2
                  DO JJ=II,nuo2
                     GF_iter_dag(JJ,II)=DCONJG(GF_iter(II,JJ))
                  ENDDO
               ENDDO
! *****************************************************************

! *****************************************************************
! Calculate the Gamma matrix for the right lead in order to
! calculate the non-equilibrium Green's function.
               Tau1_aux=(0.D0,0.D0)
               DO II=1,NR2
                  DO JJ=1,NR2
                     DO j = 1,2
                        DO k = 1,2
                           Tau1_aux(j*N12-NR2+II,k*N12-NR2+JJ)=
     &                          (fR-fL)*zi*
     &                          (Sigma_R((j-1)*NR2+II,(k-1)*NR2+JJ,
     &                          I+N_IVPOINTS)-
     &                          DCONJG(Sigma_R((k-1)*NR2+JJ,
     &                          (j-1)*NR2+II,I+N_IVPOINTS)))
                        ENDDO
                     ENDDO
                  ENDDO
               ENDDO

               CALL ZHEMM('R','U',nuo2,nuo2,(1.D0,0.D0),Tau1_aux,nuo2,
     &              GF_iter,nuo2,(0.D0,0.D0),aux,nuo2)
               CALL ZGEMM('N','N',nuo2,nuo2,nuo2,(1.D0,0.D0),aux,nuo2,
     &              GF_iter_dag,nuo2,(0.D0,0.D0),GF_iter,nuo2)
! ******************************************************************

! ******************************************************************
! Calculate the "out of equilibrium" contribution to the charge.
               DO II=1,N12
                  DO JJ=1,N12
                     IF (((II .GT. NL2) .AND. (II .LE. N12-NR2)) .OR.
     &                    ((JJ .GT. NL2) .AND. (JJ .LE. N12-NR2))) THEN
                        DO j = 1,2
                           DO k = 1,2
                              Deltarho((j-1)*N12+II,(k-1)*N12+JJ)=
     &                             Deltarho((j-1)*N12+II,(k-1)*N12+JJ) + 
     &                             1.D0/(2.D0*PI)*dE*CONST*
     &                             GF_iter((j-1)*N12+II,(k-1)*N12+JJ)
                           ENDDO
                        ENDDO
                     ENDIF
                  ENDDO
               ENDDO

               IF (MOD(iscf-1,10) .EQ. 0) THEN
                  DO jj = 1,2
                     DO k = 1,2
                        WRITE(109,'(4f13.8)') Ei,
     &                       DREAL(GF_iter((jj-1)*N12+NL2+1,
     &                       (k-1)*N12+NL2+1)),
     &                       DREAL(GF_iter(jj*N12-NR2,k*N12-NR2)),dE
                     ENDDO
                  ENDDO
               ENDIF
! ******************************************************************

            ENDIF            
         ENDDO
!        ### End of Energy Loop out of equilibrium ###

         rhoG=rho0+Deltarho

         DEALLOCATE(GF_iter,GF_iter_dag,aux,Sigma_aux
     &        ,Tau1_aux,Deltarho,rho0)
         DEALLOCATE(IPIV,WORK)
         CALL MEMORY('D','Z',6*nuo2*nuo2,'NEGF2G')
         CALL MEMORY('D','D',2*nuo2*nuo2,'NEGF2G')
         CALL MEMORY('D','I',nuo2,'NEGF2G')
         CALL MEMORY('D','Z',2*nuo2,'NEGF2G')
 
      ELSE
! ####################################################################
!        If the density matrix has converged, we calculate the current.

         IF (TrCoeff) THEN
            CALL TIMER('TRANSM',1)
            CALL TRANSM(1,1,nuo2,NL,NR,1,VBias,ivv,Haux,Saux,
     &           H0_L,VHL,S0_L,VSL,H0_R,VHR,S0_R,VSR,
     &           QL,side_rankL,numberL,QR,side_rankR,numberR,
     &           4,slabel,kpoint(1:2))
            CALL TIMER('TRANSM',2) 
         ENDIF

         CALL TIMER('CURRENT',1)

         ALLOCATE(GF_iter(nuo2,nuo2),Sigma_aux(nuo2,nuo2))
         ALLOCATE(Gamma_L(NL,NL),Gamma_R(NR,NR),Ic_aux(NL),
     &        Gamma1_aux(NL,NR),Gamma2_aux(NR,NL),
     &        GF_iter_dag(nuo2,nuo2))
         ALLOCATE(IPIV(nuo2),WORK(nuo2**2))
 
         Ic=0.D0

         DO I=1,NEnergR
            IF ((I .GE. IVSTART) .AND. (I .LE. IVFINISH)) THEN
               Ei=EnergI+(I-1)*dE

               IF ((Ei-Ef_Lead-e*VBias/2.D0)/(kB*temp) .GT. 40.D0) THEN
                  fL=0.D0
               ELSE
                  fL=1.D0/(1+CDEXP((Ei+zi*imDelta-Ef_Lead-e*VBias/2.D0)
     &                 /(kB*temp)))
               ENDIF

               IF ((Ei-Ef_Lead+e*VBias/2.D0)/(kB*temp) .GT. 40.D0) THEN
                  fR=0.D0
               ELSE
                  fR=1.D0/(1+CDEXP((Ei+zi*imDelta-Ef_Lead+e*VBias/2.D0)
     &                 /(kB*temp)))
               ENDIF

               IF ((I .EQ. IVSTART) .OR. (I .EQ. IVFINISH)) THEN
                  CONST=1.0D0/3.0D0
               ELSE
                  IF (MOD(I-IVSTART+1,2) .EQ. 0) THEN
                     CONST=4.D0/3.D0
                  ELSE
                     CONST=2.D0/3.D0
                  ENDIF
               ENDIF

               Sigma_aux=(0.D0,0.D0) 
               DO j = 1,2
                  DO k = 1,2
                     Sigma_aux((j-1)*N12+1:(j-1)*N12+NL2,
     &                    (k-1)*N12+1:(k-1)*N12+NL2)=
     &                    Sigma_L((j-1)*NL2+1:j*NL2,(k-1)*NL2+1:k*NL2,
     &                    I-N_IVPOINTS)
                  ENDDO
               ENDDO
               IF ((NL2 .EQ. N12) .AND. (NR2 .EQ. N12)) THEN
                  DO j = 1,2
                     DO k = 1,2
                        Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                       (k-1)*N12+1:(k-1)*N12+NR2)=
     &                       Sigma_aux((j-1)*N12+1:(j-1)*N12+NR2,
     &                       (k-1)*N12+1:(k-1)*N12+NR2)+
     &                       Sigma_R((j-1)*NR2+1:
     &                       j*NR2,(k-1)*NR2+1:k*NR2,I+N_IVPOINTS)
                     ENDDO
                  ENDDO
               ELSE
                  DO j = 1,2
                     DO k = 1,2
                        Sigma_aux(j*N12-NR2+1:j*N12,k*N12-NR2+1:k*N12)=
     &                       Sigma_R((j-1)*NR2+1:
     &                       j*NR2,(k-1)*NR2+1:k*NR2,I+N_IVPOINTS)
                     ENDDO
                  ENDDO
               ENDIF

               GF_iter=(Ei+zi*imDelta)*Saux-Haux-Sigma_aux

               CALL ZGETRF(nuo2,nuo2,GF_iter,nuo2,IPIV,INFO)
               CALL ZGETRI(nuo2,GF_iter,nuo2,IPIV,WORK,nuo2**2,INFO)

               DO II=1,nuo2
                  DO JJ=II,nuo2
                     GF_iter_dag(II,JJ)=DCONJG(GF_iter(JJ,II))
                  ENDDO
               ENDDO

               DO II=1,MAX(NR,NL)
                  DO JJ=II,MAX(NR,NL)
                     IF ((II .LE. NL) .AND. (JJ .LE. NL))
     &                    Gamma_L(II,JJ)=
     &                    zi*(Sigma_L(II,JJ,I-N_IVPOINTS)-
     &                    DCONJG(Sigma_L(JJ,II,I-N_IVPOINTS)))
                     IF ((II .LE. NR) .AND. (JJ .LE. NR))
     &                    Gamma_R(II,JJ)=
     &                    zi*(Sigma_R(II,JJ,I+N_IVPOINTS)-
     &                    DCONJG(Sigma_R(JJ,II,I+N_IVPOINTS)))
                  ENDDO
               ENDDO

               ALLOCATE(GF1_aux(NL,NR),GF2_aux(NR,NL))
               DO j=1,2
                  DO k=1,2
                     GF1_aux((j-1)*NL2+1:j*NL2,(k-1)*NR2+1:k*NR2) =
     &                    GF_iter_dag((j-1)*N12+1:
     &                    (j-1)*N12+NL2,k*N12-NR2+1:k*N12)
                     GF2_aux((j-1)*NR2+1:j*NR2,(k-1)*NL2+1:k*NL2) =
     &                    GF_iter(j*N12-NR2+1:
     &                    j*N12,(k-1)*N12+1:(k-1)*N12+NL2)
                  ENDDO
               ENDDO
               CALL ZHEMM('L','U',NL,NR,(1.D0,0.D0),Gamma_L,NL,
     &              GF1_aux,NL,(0.D0,0.D0),Gamma1_aux,NL)
               CALL ZHEMM('L','U',NR,NL,(1.D0,0.D0),Gamma_R,NR,
     &              GF2_aux,NR,(0.D0,0.D0),Gamma2_aux,NR)
               DEALLOCATE(GF1_aux,GF2_aux)

               DO J=1,NL
                  Ic_aux(J)=0.D0
                  DO L=1,NR
                     Ic_aux(J)=Ic_aux(J)+Gamma1_aux(J,L)*Gamma2_aux(L,J)
                  ENDDO
               ENDDO

               DO J=1,NL
                  Ic=Ic+eh_const*dE*DREAL(CONST*Ic_aux(J)*(fL-fR))
               ENDDO
 
            ENDIF
         ENDDO

         call io_assign(iuc)
         curfile = paste(slabel,'.CUR')
         If (ivv .EQ. 0) Then
            OPEN(UNIT=iuc,FILE=curfile)
         Else
            OPEN(UNIT=iuc,FILE=curfile,POSITION='append')
         EndIf
!        VBias given in eV (from CODATA - 2012).
         WRITE(iuc,'(2f14.8)') VBias*13.60569253, Ic
         call io_close(iuc)

         IF (ALLOCATED(ZSigma_R))
     &        DEALLOCATE(ZSigma_R,ZSigma_L,EContour,WContour) 
         DEALLOCATE(GF_iter,Sigma_aux,
     &        Ic_aux,Gamma_L,Gamma_R,Gamma1_aux,Gamma2_aux,GF_iter_dag)
         DEALLOCATE(IPIV,WORK)
         CALL TIMER('CURRENT',2)
      ENDIF

      CALL TIMER('NEGF2G',2)

      return
      end

