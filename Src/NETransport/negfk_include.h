C ********* Variables *************************************
	
	IMPLICIT NONE

C ********** Parameters ***********************************
	INTEGER :: N1,NSPIN,nsc(2),
     &   nspinL,maxnhL,nspinR,maxnhR,
     &   Nenerg,Nenerg1,Nenerg2,NPOLES,ITER,istep,ICASE
	INTEGER, SAVE :: NL,NR,maxdepth,ndivisions
        INTEGER, SAVE :: N_IVPOINTS,IVSTART,IVFINISH
	LOGICAL, SAVE :: PERIODIC
	CHARACTER(LEN=15), SAVE :: integraltype
	CHARACTER(LEN=12), SAVE :: gridmethod
	LOGICAL, SAVE :: SaveMemory,Gamma
        INTEGER, SAVE :: sigma_method
        DOUBLE PRECISION, SAVE :: deltaimag
	
	INCLUDE "const.h"

C ******** Auxiliaries ******************************************************
	DOUBLE COMPLEX, ALLOCATABLE :: Tau1_aux(:,:),rho_aux(:,:)
	DOUBLE COMPLEX, ALLOCATABLE :: aux(:,:)
	
C ******** Labels ***********************************************************
	CHARACTER slabel*20,slabelL*20,slabelR*20,paste*25,curfile*25
	
C ******** Constant arrays **************************************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: ZSigma_L(:,:,:,:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: ZSigma_R(:,:,:,:,:)

C ****** Charge Density *****************************************************
	DOUBLE COMPLEX, DIMENSION (N1,N1,NSPIN) :: rhoG

C ********** Leads H0,H1,S0,S1 ****************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: H0_L(:,:,:),H1_L(:,:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: S0_L(:,:),S1_L(:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: H0_R(:,:,:),H1_R(:,:,:)
	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: S0_R(:,:),S1_R(:,:)

C ********** Decimation Variables *************************
	DOUBLE COMPLEX, ALLOCATABLE, SAVE, DIMENSION (:,:,:,:) ::
     .    QR,QL,VHL
	DOUBLE COMPLEX, ALLOCATABLE, SAVE, DIMENSION (:,:,:,:) ::
     .    VHR,VSL,VSR
	INTEGER, ALLOCATABLE,DIMENSION (:,:), SAVE :: numberL,numberR
C ********** Scaterer Hamiltonian *************************
	DOUBLE COMPLEX, DIMENSION (N1,N1,NSPIN) :: H_Chain
	DOUBLE COMPLEX, DIMENSION (N1,N1) :: S0_Chain

C ********** Energy Related Variables  ********************

C Energy
	DOUBLE PRECISION :: Ei,Delta,EB,kbt
	DOUBLE PRECISION,SAVE :: EnergI,EnergF,Ef_Lead,dE,R0,
     &                                Ef_LeadL,Ef_LeadR
	DOUBLE PRECISION :: Vini,Vfinal
C Temperature
	DOUBLE PRECISION :: T	
C Potencial bias
        DOUBLE PRECISION :: V,wk
        DOUBLE PRECISION, DIMENSION (2), SAVE :: Ic
        DOUBLE PRECISION, DIMENSION (NSPIN) :: Icfinal


	DOUBLE COMPLEX, ALLOCATABLE, SAVE :: EContour(:),WContour(:)
	
	INTEGER :: IV
	INTEGER :: I,J,II,JJ,ISPIN,L,iu,iuc,nrchan,nrchanR,nrchanL
	INTEGER :: iktot,iik,ik,nk
	DOUBLE PRECISION, DIMENSION (3) :: kpoint
	INTEGER :: INFO,NNSPIN,NSLICES
	LOGICAL :: CONVERGED,CALCTRANSM

C ******* Parallel Variables ******************************
	INTEGER,SAVE :: Nenerg_div,NenergIm_div,NenergImNode,Nsmall_div,
     &   Nenerg_div_nodes
        INTEGER :: Nnodes,MyNode,Inode,Iprime
	DOUBLE PRECISION, SAVE :: EnergIntMin,EnergIntMax

