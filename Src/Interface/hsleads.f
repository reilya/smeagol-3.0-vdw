!  *******************************************************************  !
!  Copyright (c) Smeagol Authors (2003-2005):  A. R. Rocha,             !
!                                              V. Garcia-Suarez,        !
!                                              S. Bailey,               !
!                                              C. J. Lambert,           !
!                                              J. Ferrer and            !
!                                              S. Sanvito               !
!                                                                       !
!  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS  !
!  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT    !
!  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS    !
!  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE      !
!  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,  !
!  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES             !
!  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR   !
!  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)   !
!  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,  !
!  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)        !
!  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  !
!  OF THE POSSIBILITY OF SUCH DAMAGE.                                   !
!                                                                       !
!  SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE              !
!  (www.smeagol.tcd.ie) UPON COMPLETION OF "SMEAGOL ACADEMIC LICENSE".  !
!                                                                       !
!  FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL smeagol@tcd.ie  !
!  *******************************************************************  !

      subroutine hsleads (trnspin, nspin, dnuoL, dnuoR, nsc, iter,
     .     istep, iv, gamma, ik, kpoint, temp,
     .     H0L, H1L, S0L, S1L, H0R, H1R, S0R, S1R)
!  *******************************************************************  !
!                                hsleads                                !
!  *******************************************************************  !
!  Description: Reads and calculates the Hamiltonians, overlaps and     !
!  density matrices of the leads.                                       !
!                                                                       !
!  Written by Victor M. Garcia-Suarez, Nov 2003.                        !
!  Departamento de Fisica                                               !
!  Universidad de Oviedo                                                !
!  e-mail: victor@condmat.uniovi.es                                     !
!  ***************************** HISTORY *****************************  !
!  Original version:    November 2003                                   !
!  Modified version:    November 2012 (modified to read overlap and     !
!                       Hamiltonian matrices from binary file by        !
!                       Pedro Brandimarte, Universidade de Sao Paulo,   !
!                       e-mail: brandimarte@gmail.com)                  !
!  ****************************** INPUT ******************************  !
!  For simplicity, it is listed only the left lead variables.           !
!  integer trnspin             : True value of the spin                 !
!  integer nspin               : Number of spin components              !
!  integer dnuoL               : Number of basis orbitals in the left   !
!                                lead, including spin components        !
!  integer dnuoR               : Number of basis orbitals in the right  !
!                                lead, including spin components        !
!  integer nsc(2)              : Number of unit cells along parallel    !
!                                directions                             !
!  integer iter                : Scf iteration in SIESTA                !
!  integer istep               : Molecular dynamics iteration           !
!  integer iv                  : Bias potential iteration               !
!  logical gamma               : Calculation with parallel k-points     !
!  integer ik                  : k-point index                          !
!  real*8  kpoint(3)           : Current parallel k point               !
!  real*8  temp                : Electronic temperature                 !
!  *********************** INPUT FROM MODULES ************************  !
!  integer inicoor             : Initial step in geommetry iteration    !
!  logical InitTransport       : Expands the supercell size to '3' in   !
!                                transversal direction?                 !
!  ***************************** OUTPUT ******************************  !
!  complex*8 H0L(dnuoL,dnuoL,nspin) : Hamiltonian in the unit cell of   !
!                                     the left lead                     !
!  complex*8 H1L(dnuoL,dnuoL,nspin) : Conexion between unit cells in    !
!                                     the left lead                     !
!  complex*8 S0L(dnuoL,dnuoL)       : Overlaps in the unit cell of      !
!                                     the left lead                     !
!  complex*8 S1L(dnuoL,dnuoL)       : Overlaps between unit cells in    !
!                                     the left lead                     !
!  complex*8 H0R(dnuoR,dnuoR,nspin) : Hamiltonian in the unit cell of   !
!                                     the right lead                    !
!  complex*8 H1R(dnuoR,dnuoR,nspin) : Conexion between unit cells in    !
!                                     the right lead                    !
!  complex*8 S0R(dnuoR,dnuoR)       : Overlaps in the unit cell of      !
!                                     the right lead                    !
!  complex*8 S1R(dnuoR,dnuoR)       : Overlaps between unit cells in    !
!                                     the right lead                    !
!  *******************************************************************  !

!
!   Modules
!
      use precision,       only: dp
      use m_steps,         only: inicoor
      use smeagol_options, only: InitTransport
      use m_memory,        only: memory

      implicit none

!     Input variables.
      integer :: trnspin, nspin, dnuoL, dnuoR, iter, istep, iv, ik
      integer :: nsc(2)
      real(dp) :: temp
      real(dp) :: kpoint(3)
      complex(dp) :: H0L(dnuoL,dnuoL,nspin), H1L(dnuoL,dnuoL,nspin)
      complex(dp) :: S0L(dnuoL,dnuoL), S1L(dnuoL,dnuoL)
      complex(dp) :: H0R(dnuoR,dnuoR,nspin), H1R(dnuoR,dnuoR,nspin)
      complex(dp) :: S0R(dnuoR,dnuoR), S1R(dnuoR,dnuoR)
      logical :: gamma
      character :: slabelL*20, slabelR*20

!     Local variables.
      integer :: nspinL, nspinR, iu, iu1, iu2, io, iuo, ind, j
      integer :: nscL(2), nscR(2)
      integer, save :: nuoL, nuoR, noL, noR, maxnhL, maxnhR
      integer, allocatable, save :: numhL(:), numhR(:),
     .                              listhptrL(:), listhptrR(:),
     .                              indxuoL(:), indxuoR(:),
     .                              listhL(:), listhR(:) 
      real(dp) :: efL, efR, tempR, tempL
      real(dp), allocatable, save :: xijL(:,:), xijR(:,:),
     .                               SL(:), SR(:), HL(:,:), HR(:,:)
      character :: paste*25
      external :: io_assign, io_close, hsl, hslk


      if (iter.eq.1 .and. istep.eq.inicoor
     .     .and. iv.eq.0 .and. ik.eq.1) then
!        Read data.
         call io_assign(iu1)
         open(iu1,file='bulklft.DAT',status='old')
         read(iu1,*) slabelL, nuoL, nspinL, maxnhL, efL, tempL,
     .        nscL(1), nscL(2), noL

         call io_assign(iu2)
         open(iu2,file='bulkrgt.DAT',status='old')
         read(iu2,*) slabelR, nuoR, nspinR, maxnhR, efR, tempR,
     .        nscR(1), nscR(2), noR

!        Allocate arrays.
         allocate(numhL(nuoL),listhptrL(nuoL))
         call memory('A','I',2*nuoL,'hsleads')
         allocate(indxuoL(noL))
         call memory('A','I',noL,'hsleads')
         allocate(listhL(maxnhL))
         call memory('A','I',maxnhL,'hsleads')
         allocate(xijL(3,maxnhL))
         call memory('A','D',3*maxnhL,'hsleads')

         allocate(numhR(nuoR),listhptrR(nuoR))
         call memory('A','I',2*nuoR,'hsleads')
         allocate(indxuoR(noR))
         call memory('A','I',noR,'hsleads')
         allocate(listhR(maxnhR))
         call memory('A','I',maxnhR,'hsleads')
         allocate(xijR(3,maxnhR))
         call memory('A','D',3*maxnhR,'hsleads')

         allocate(SL(maxnhL))
         call memory('A','D',maxnhL,'hsleads')
         allocate(SR(maxnhR))
         call memory('A','D',maxnhR,'hsleads')
         allocate(HL(maxnhL,trnspin))
         call memory('A','D',maxnhL*trnspin,'hsleads')
         allocate(HR(maxnhR,trnspin))
         call memory('A','D',maxnhR*trnspin,'hsleads')

!       Read data of the left lead.
        do iuo = 1, nuoL
           read(iu1,*) numhL(iuo), listhptrL(iuo)
        enddo
        do io = 1, noL
           read(iu1,*) indxuoL(io)
        enddo
        do iuo = 1, nuoL
           do j = 1, numhL(iuo)
              ind = listhptrL(iuo) + j
              read(iu1,*) listhL(ind)
              if (InitTransport .or. .not.gamma) then
                 read(iu1,*) xijL(1,ind), xijL(2,ind), xijL(3,ind)
              endif
           enddo
        enddo
      
!       Read data of the right lead.
        do iuo = 1, nuoR
           read(iu2,*) numhR(iuo), listhptrR(iuo)
        enddo
        do io = 1, noR
           read(iu2,*) indxuoR(io)
        enddo
        do iuo = 1, nuoR
           do j = 1, numhR(iuo)
              ind = listhptrR(iuo) + j
              read(iu2,*) listhR(ind)
              if (InitTransport .or. .not.gamma) then
                 read(iu2,*) xijR(1,ind), xijR(2,ind), xijR(3,ind)
              endif
           enddo
        enddo
      
        call io_close(iu1)
        call io_close(iu2)

!       Compare supercell of leads and EM.
        if (nsc(1).ne.nscL(1) .or. nsc(2).ne.nscL(2)) then
           write(6,'(a)') 'ERROR: The left supercell along parallel'
           write(6,'(a)') 'directions is different of the supercell'
           write(6,'(a)') 'of the EM. Change the size of it'
           write(6,'(a,2i4)') 'nsc = ', nsc(1), nsc(2)
           write(6,'(a,2i4)') 'nscL = ', nscL(1), nscL(2)
        endif
        if (nsc(1).ne.nscR(1) .or. nsc(2).ne.nscR(2)) then
           write(6,'(a)') 'ERROR: The right supercell along parallel'
           write(6,'(a)') 'directions is different of the supercell'
           write(6,'(a)') 'of the EM. Change the size of it'
           write(6,'(a,2i4)') 'nsc = ', nsc(1), nsc(2)
           write(6,'(a,2i4)') 'nscR = ', nscR(1), nscR(2)
        endif
          
!       Verify if the spin at the leads and the EM is the same.
        if (nspinL.ne.trnspin) then
           write(6,'(a)') 'ERROR: The spin at the left lead is not'
           write(6,'(a)') 'the same as in the extended molecule'
           stop
        else if (nspinR.ne.trnspin) then
           write(6,'(a)') 'ERROR: The spin at the right lead is not'
           write(6,'(a)') 'the same as in the extended molecule'
           stop
        endif

!       Check the temperature.
        if (tempL-temp.gt.1.d-4) then
           write(6,'(a)')'WARNING: The temperature at the left lead is'
           write(6,'(a)')'not the same as in the extended molecule'
        endif
        if (tempR-temp.gt.1.d-4) then
           write(6,'(a)')'WARNING: The temperature at the right lead is'
           write(6,'(a)')'not the same as in the extended molecule'
        endif

!       Initialize overlaps and Hamiltonians.
        SL = 0.d0
        SR = 0.d0
        HL = 0.d0
        HR = 0.d0

!       Read overlaps and Hamiltonians.
        call io_assign(iu)
        open(iu,file=paste(slabelL,'.HST'),status='old')
        do iuo = 1, nuoL
           do j = 1, numhL(iuo) 
              ind = listhptrL(iuo) + j
              read(iu,*) SL(ind)
              read(iu,*) HL(ind,:)
           enddo
        enddo
        call io_close(iu)
        
        call io_assign(iu)
        open(iu,file=paste(slabelR,'.HST'),status='old')
        do iuo = 1, nuoR
           do j = 1, numhR(iuo) 
              ind = listhptrR(iuo) + j
              read(iu,*) SR(ind)
              read(iu,*) HR(ind,:)
           enddo
        enddo
        call io_close(iu)

c$$$C Changed to read in binary format.
c$$$        call io_assign(iu)
c$$$        open (iu, file=paste(slabelL,'.HST'), status='old',
c$$$     .       access='stream')
c$$$        do iuo = 1, nuoL
c$$$           read(iu) SL(listhptrL(iuo)+1:listhptrL(iuo)+numhL(iuo))
c$$$        enddo
c$$$
c$$$        do is = 1,nspinL
c$$$           do iuo = 1, nuoL
c$$$              read(iu) HL(listhptrL(iuo)+1:listhptrL(iuo)+numhL(iuo),is)
c$$$           enddo
c$$$        enddo
c$$$        call io_close(iu)
c$$$
c$$$        call io_assign(iu)
c$$$        open (iu, file=paste(slabelR,'.HST'), status='old',
c$$$     .       access='stream')
c$$$        do iuo = 1, nuoR
c$$$           read(iu) SR(listhptrR(iuo)+1:listhptrR(iuo)+numhR(iuo))
c$$$        enddo
c$$$
c$$$        do is = 1,nspinR
c$$$           do iuo = 1, nuoR
c$$$              read(iu) HR(listhptrR(iuo)+1:listhptrR(iuo)+numhR(iuo),is)
c$$$           enddo
c$$$        enddo
c$$$        call io_close(iu)

!       Calculate S0, S1, H0 and H1 (left and right).
        if (gamma .and. InitTransport) then
           call hslk(trnspin, nspin, nuoL, dnuoL, noL, maxnhL, numhL,
     .          listhptrL, indxuoL, listhL, xijL, kpoint, nsc,
     .          SL, HL, S0L, S1L, H0L, H1L)
           call hslk(trnspin, nspin, nuoR, dnuoR, noR, maxnhR, numhR,
     .          listhptrR, indxuoR, listhR, xijR, kpoint, nsc,
     .          SR, HR, S0R, S1R, H0R, H1R)
        else if (gamma) then
           call hsl(trnspin, nspin, nuoL, dnuoL, noL, maxnhL,
     .          numhL, listhptrL, indxuoL, listhL, SL, HL,
     .          S0L, S1L, H0L, H1L)
           call hsl(trnspin, nspin, nuoR, dnuoR, noR, maxnhR,
     .          numhR, listhptrR, indxuoR, listhR, SR, HR,
     .          S0R, S1R, H0R, H1R)
        endif
          
      endif ! if (iter.eq.1 .and. istep.eq.inicoor .and. ...


!     Calculate S0, S1, H0 and H1 (left and right) if not gamma point
      if (.not.gamma) then
         call hslk(trnspin, nspin, nuoL, dnuoL, noL, maxnhL, numhL,
     .        listhptrL, indxuoL, listhL, xijL, kpoint, nsc,
     .        SL, HL, S0L, S1L, H0L, H1L)
         call hslk(trnspin, nspin, nuoR, dnuoR, noR, maxnhR, numhR,
     .        listhptrR, indxuoR, listhR, xijR, kpoint, nsc,
     .        SR, HR, S0R, S1R, H0R, H1R)
      endif

      end

