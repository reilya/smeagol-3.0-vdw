!  *******************************************************************  !
!  Copyright (c) Smeagol Authors (2003-2005):  A. R. Rocha,             !
!                                              V. Garcia-Suarez,        !
!                                              S. Bailey,               !
!                                              C. J. Lambert,           !
!                                              J. Ferrer and            !
!                                              S. Sanvito               !
!                                                                       !
!  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS  !
!  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT    !
!  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS    !
!  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE      !
!  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,  !
!  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES             !
!  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR   !
!  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)   !
!  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,  !
!  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)        !
!  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  !
!  OF THE POSSIBILITY OF SUCH DAMAGE.                                   !
!                                                                       !
!  SMEAGOL IS DISTRIBUTED ONLY THROUGH THE OFICIAL WEBSITE              !
!  (www.smeagol.tcd.ie) UPON COMPLETION OF "SMEAGOL ACADEMIC LICENSE".  !
!                                                                       !
!  FOR INFORMATION OR QUERIES PLEASE CONTACT THE E-MAIL smeagol@tcd.ie  !
!  *******************************************************************  !

      subroutine emt2g (nuo, iscf, last, istep, ivv, VBias, Haux, Saux)
!  *******************************************************************  !
!                                 emt2g                                 !
!  *******************************************************************  !
!  Description: subroutine to calculate the transport in the extended   !
!  molecule. Gamma-point version for non-polarized or spin-polarized    !
!  Hamiltonians.                                                        !
!                                                                       !
!  Written by V. M. Garcia-Suarez, Sep 2003.                            !
!  Departamento de Fisica                                               !
!  Universidad de Oviedo                                                !
!  e-mail: victor@condmat.uniovi.es                                     !
!  ***************************** HISTORY *****************************  !
!  Original version:    September 2003                                  !
!  Modified version:    November 2012 (transitioned to SIESTA 3.1 by    !
!                       Pedro Brandimarte, Universidade de Sao Paulo,   !
!                       e-mail: brandimarte@gmail.com)                  !
!  ****************************** INPUT ******************************  !
!  integer nuo             : Number of basis orbitals local to node     !
!  integer iscf            : SCF iteration                              !
!  logical last            : If last SCF iteration, calculate the       !
!                            current                                    !
!  integer istep           : Molecular dynamics loop                    !
!  integer ivv             : Bias potential step                        !
!  real*8 VBias            : Bias potential                             !
!  *********************** INPUT FROM MODULES ************************  !
!  integer nspin           : Number of spin components (1 or 2)         !
!  integer numh(no_u)      : Number of nonzero elements of each row     !
!                            of Hamiltonian matrix                      !
!  integer listhptr(no_u)  : Pointer to each row (-1) of the            !
!                            Hamiltonian matrix                         !
!  integer listh(maxnh)    : Nonzero hamiltonian-matrix element         !
!                            column indexes for each matrix row         !
!  real*8 H(maxnh,nspin)   : Hamiltonian in sparse form                 !
!  real*8 S(maxnh)         : Overlap in sparse form                     !
!  integer nsc(3)          : Number of unit cells along x, y, and z     !
!  integer inicoor         : Initial step in geommetry iteration        !
!  ******************** INPUT FROM MODULES/OUTPUT ********************  !
!  real*8 Dscf(maxnh,nspin) : Output density matrix                     !
!  *******************************************************************  !

!
!   Modules
!
      use precision,       only: dp
      use m_spin,          only: nspin
      use sparse_matrices, only: numh, listhptr, listh, H, S, Dscf
      use siesta_geom,     only: nsc
      use m_steps,         only: inicoor
      use smeagol_options, only: MDsteps
      use fdf

      implicit none

!     Input variables.
      integer :: nuo, iscf, istep, ivv
      real(dp) :: VBias
      complex(dp) :: Haux(2*nuo,2*nuo), Saux(2*nuo,2*nuo)
      logical :: last

!     Internal variables.
      integer :: iuo, j, juo, ind, iu1, iu2, nspinL, nspinR,
     .           juoL, jL, indL, juoR, jR, indR
      integer, save :: nuoR, nuoL, maxnhL, maxnhR
      integer, allocatable, save :: numdL(:), listdptrL(:), listdL(:),
     .                              numdR(:), listdptrR(:), listdR(:)
      real(dp), allocatable, save :: DMbulkL(:,:), DMbulkR(:,:)
      complex(dp) :: ii
      complex(dp), allocatable :: rhoG(:,:)
      logical, save :: periodic
      character*20 :: slabelL, slabelR
      external :: negf2g, memory, io_assign, io_close, dmbk

      ii = (0.d0, 1.d0)

      allocate(rhoG(2*nuo,2*nuo))
      call memory('A','Z',8*nuo*nuo,'emt2g')

!     Find Hamiltonian and overlap .
      Saux = 0.0d0
      Haux = 0.0d0
      do iuo = 1,nuo
         do j = 1,numh(iuo)
            ind = listhptr(iuo) + j
            juo = listh(ind)
            Saux(juo,iuo) = S(ind)
            Saux(nuo+juo,nuo+iuo) = S(ind)
            Haux(juo,iuo) = H(ind,1)
            Haux(juo,nuo+iuo) = H(ind,3) - ii*H(ind,4)
            Haux(nuo+juo,nuo+iuo) = H(ind,2)
         enddo
      enddo

!     Hermiticity of H.
      do iuo = 1,nuo
         do juo = 1,nuo
            Haux(nuo+juo,iuo) = dconjg(Haux(iuo,nuo+juo))
         enddo
      enddo

!     Transport subroutine.
      call negf2g(2*nuo,nsc(1:2),istep,ivv,iscf,VBias,
     .     Haux,Saux,last,rhoG)

!     New density matrix.
      Dscf = 0.d0
      do iuo = 1,nuo
         do j = 1,numh(iuo)
            ind = listhptr(iuo) + j
            juo = listh(ind)
            Dscf(ind,1) = rhoG(juo,iuo)
            Dscf(ind,3) = dreal(rhoG(juo,nuo+iuo))
            Dscf(ind,4) = dimag(rhoG(juo,nuo+iuo))
            Dscf(ind,2) = rhoG(nuo+juo,nuo+iuo)
         enddo
      enddo

!     Add bulk density matrix.
      if (iscf.eq.1 .AND. istep.eq.inicoor .AND. ivv.eq.0) then

         call io_assign(iu1)
         open(iu1,file='bulklft.DAT',status='old')
         read(iu1,*) slabelL, nuoL, nspinL, maxnhL

         call io_assign(iu2)
         open(iu2,file='bulkrgt.DAT',status='old')
         read(iu2,*) slabelR, nuoR, nspinR, maxnhR
 
         allocate(DMbulkL(maxnhL,nspin))
         call memory('A','Z',maxnhL*nspin,'emt2g')
         allocate(DMbulkR(maxnhR,nspin))
         call memory('A','Z',maxnhR*nspin,'emt2g')

         allocate(numdL(nuoL),listdptrL(nuoL),listdL(maxnhL))
         call memory('A','I',2*nuoL+maxnhL,'emt2g')
         allocate(numdR(nuoR),listdptrR(nuoR),listdR(maxnhR))
         call memory('A','I',2*nuoR+maxnhR,'emt2g')

         DMbulkL = 0.d0
         call dmbk(nuoL,maxnhL,nspin,slabelL,
     .        numdL,listdL,listdptrL,DMbulkL)
         DMbulkR = 0.d0
         call dmbk(nuoR,maxnhR,nspin,slabelR,
     .        numdR,listdR,listdptrR,DMbulkR)

         call io_close(iu1)
         call io_close(iu2)
      
         periodic = fdf_boolean('PeriodicTransp',.true.)

      endif

      do iuo = 1,nuoL
         do j = 1,numh(iuo)
            ind = listhptr(iuo) + j
            juo = listh(ind)
            if (juo .le. nuoL) then
               indL = listdptrL(iuo) + 1
               juoL = listdL(indL)
               jL = 1
               do while((juoL.ne.juo) .and. (jL .lt. numdL(iuo)))
                  jL = jL + 1
                  indL = listdptrL(iuo) + jL
                  juoL = listdL(indL)
               enddo
               if (juoL .eq. juo)
     .              Dscf(ind,:) = DMbulkL(indL,:)
            endif
            if ((juo.gt.nuo-nuoR).and.periodic) then
               indL = listdptrL(iuo) + 1
               juoL = listdL(indL)
               jL = 1
               do while (((juoL-2*nuoL).ne.(juo-(nuo-nuoR))) .and.
     .              (jL.lt.numdL(iuo)))
                  jL = jL + 1
                  indL = listdptrL(iuo) + jL
                  juoL = listdL(indL)
               enddo
               if ((juoL-2*nuoL) .eq. (juo-(nuo-nuoR)))
     .              Dscf(ind,:) = DMbulkL(indL,:)
            endif
         enddo
      enddo

      do iuo = nuo-nuoR+1,nuo
         do j = 1,numh(iuo)           
            ind = listhptr(iuo) + j
            juo = listh(ind)
            if (juo.gt.nuo-nuoR) then
               indR = listdptrR(iuo-(nuo-nuoR)) + 1
               juoR = listdR(indR)
               jR = 1
               do while((juoR.ne.(juo-(nuo-nuoR))) .and.
     .              (jR.lt.numdR(iuo-(nuo-nuoR))))
                  jR = jR + 1
                  indR = listdptrR(iuo-(nuo-nuoR)) + jR
                  juoR = listdR(indR)
               enddo
               if (juoR .eq. (juo-(nuo-nuoR)))
     .              Dscf(ind,:) = DMbulkR(indR,:)
            endif
            if (juo.le.nuoL .and. periodic) then
               jR = 1
               indR = listdptrR(iuo-(nuo-nuoR)) + 1
               juoR = listdR(indR)
               do while (((juoR-nuoL).ne.juo) .and.
     .              (jR.lt.numdR(iuo-(nuo-nuoR))))
                  jR = jR + 1 
                  indR = listdptrR(iuo-(nuo-nuoR)) + jR
                  juoR = listdR(indR)
               enddo
               if ((juoR-nuoL) .eq. juo)
     .              Dscf(ind,:) = DMbulkR(indR,:)
            endif
         enddo
      enddo

!     Free local arrays.
      call memory('D','Z',size(rhoG),'emt2g')
      deallocate(rhoG)
      
      return
      end

