!     Constants.
      double precision, parameter :: eh_const = 1.602176565D-4
     .                                   * 13.60569253D0 / 4.135667516D0
      double precision, parameter :: e       = 1.D0
      double precision, parameter :: kB      = 1.0D0
      double precision, parameter :: PI      = 3.141592653589793238462D0
      double complex, parameter ::   zi      = (0.D0,1.D0)

!     Tolerance.
      double precision, parameter :: TOL     = 5.D-5
      double precision, parameter :: kbTol   = 18.0D0
      double complex, parameter ::   alpha   = (1.0d0,0.d0)
      double complex, parameter ::   beta    = (0.0d0,0.d0)
