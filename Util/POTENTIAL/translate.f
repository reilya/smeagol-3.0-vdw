      SUBROUTINE translate (n,aver,daver,unit,bound,nspin)

C Translate the planar and linear average by 
C a specified ammount
C WRITTEN BY S.Sanvito  FEB/01

      IMPLICIT none

	integer n,unit,pos,i,l,bound,nspin,is
	real aver(n,2),daver(n,2),use(n,2)

	do is = 1,nspin
	 do l=1,bound
	  pos=l+unit
	  if(pos.gt.bound) pos=pos-bound 
	  use(l,is)=aver(pos,is)
	 enddo
        enddo

	do is = 1,nspin
	 do l=1,bound
	  aver(l,is)=use(l,is)
	 enddo
        enddo

	do is = 1,nspin
 	 do l=1,bound
	  pos=l+unit
	  if(pos.gt.bound) pos=pos-bound 
	  use(l,is)=daver(pos,is)
	 enddo
        enddo

	do is = 1,nspin
	 do l=1,bound
	  daver(l,is)=use(l,is)
	 enddo
        enddo

      END

