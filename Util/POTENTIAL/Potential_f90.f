      program Potential_f90

c Extract the total potential and calculate potential profile	      
c
c Written by S.Sanvito Feb 2001

      implicit none

c Common arrays

      include 'Potential.h'

c Internal parameters
c maxp   : Maximun number of points
      integer maxp,maxort
      real ryd,shift,eshift
      parameter ( maxp   = 100000000 )
      parameter ( maxort   = 10000000 )
      parameter ( ryd   = 13.6058d0 )

c Internal variables
      integer lengfn
      character
     .  name*75,fform*12,fname*80,output*20,paste*80,task*15,foutto*80,
     .  foutav*80,TR*15 
      integer
     .  i,i1,i2,i3,icmin,icmax,ip,is,j,l,pos,k,mul,period,per,ir,izm,
     .  mesh(3), ncolor, np, nspin, nt, pgopen, naver,ntr_shift,indi
      real
     .  alpha, beta, cell(3,3), colavg, colmax, colmin, f1(maxp,2),
     .  f(maxp,2), fvalue, f1max, f1min, f2max, f2min, f2zero,
     .  gamma, origin(3), rho, rx, ry,
     .  vpxmax, vpxmin, vsxmax, vsxmin,
     .  vpymax, vpymin, vsymax, vsymin,
     .  x, xmax, xmean, xmin, xrange,
     .  y, ymax, ymean, ymin, yrange,unit,
     .  ni,nj,nl,aver(maxort,2),norm,tr_shift
      real 
     .   daver(maxort,2),laver   
      double precision
     .  dcell(3,3)
      external paste,rotate
      
      data
     .  ncolor /0/
     .  origin /3*0.0/
   
c Read plot data
      open( unit=1, file='Potential.dat',
     .      status='old', form='formatted' )
      read(1,*) name
      read(1,*) task
      read(1,*) alpha, beta, gamma
      read(1,*) fvalue
      read(1,*) f2min, f2zero, f2max
      read(1,*) fform
      read(1,*) output
      read(1,*) laver
      read(1,*) shift
      read(1,*) eshift
      read(1,*) TR
      read(1,*) tr_shift
      read(1,*) period
      close(1)

c Read potential

      if (task.eq.'vt' .or. task.eq.'vh' .or. task.eq.'rho') then
        ncolor = 1
         if (task .eq. 'vt') then
	  lengfn = index(name,' ') - 1
          fname = name(1:lengfn)//'.VT'
          foutto = name(1:lengfn)//'-VT.dat'
          foutav = name(1:lengfn)//'-VT_AV.dat'
         elseif (task .eq. 'vh') then
	  lengfn = index(name,' ') - 1
          fname = name(1:lengfn)//'.VH'
          foutto = name(1:lengfn)//'-VH.dat'
          foutav = name(1:lengfn)//'-VH_AV.dat'
         elseif (task .eq. 'rho') then
	  lengfn = index(name,' ') - 1
          fname = name(1:lengfn)//'.RHO'
          foutto = name(1:lengfn)//'-RHO.dat'
          foutav = name(1:lengfn)//'-RHO_AV.dat'
	 endif 
        open( unit=1, file=fname, status='old', form=fform )
        if (fform .eq. 'formatted') then
          read(1,*) cell
          read(1,*) mesh, nspin
          np = mesh(1) * mesh(2) * mesh(3)
          read(1,*) ((f(ip,is),ip=1,np),is=1,nspin)
        else
          read(1) dcell
          read(1) mesh, nspin
	  write(6,'(a,3(/,3f12.6))') 'Potential: cell =', dcell
          write(6,'(a,3i6)') 'Potential: mesh  =', mesh
          write(6,'(a,3i6)') 'Spin:    nspin   =', nspin
          np = mesh(1) * mesh(2) * mesh(3)
	  do is = 1,nspin
	   indi=0
	   do izm = 1,mesh(3)
	    do ir = 1,mesh(2)
               read(1) (f(indi+ip,is),ip=1,mesh(1))
	       indi = indi + mesh(1)
	    enddo   
	   enddo
	  enddo
        endif
        close(1)
      elseif (task.eq.'vt-vh') then
	  lengfn = index(name,' ') - 1
          fname = name(1:lengfn)//'.VH'
          foutto = name(1:lengfn)//'-VT-VH.dat'
          foutav = name(1:lengfn)//'-VT-VH_AV.dat'
        open( unit=1, file=fname, status='old', form=fform )
        if (fform .eq. 'formatted') then
          read(1,*) cell
          read(1,*) mesh, nspin
          np = mesh(1) * mesh(2) * mesh(3)
          read(1,*) ((f1(ip,is),ip=1,np),is=1,nspin)
        else
          read(1) dcell
          read(1) mesh, nspin
          np = mesh(1) * mesh(2) * mesh(3)
	  do is = 1,nspin
           read(1) (f1(ip,is),ip=1,np)
	  enddo
        endif
        close(1, status='keep')
          fname = name(1:lengfn)//'.VT'
        open( unit=1, file=fname, status='old', form=fform )
        if (fform .eq. 'formatted') then
          read(1,*) cell
          read(1,*) mesh, nspin
          np = mesh(1) * mesh(2) * mesh(3)
          read(1,*) ((f(ip,is),ip=1,np),is=1,nspin)
        else
          read(1) dcell
          read(1) mesh, nspin
          np = mesh(1) * mesh(2) * mesh(3)
	  do is = 1,nspin
           read(1) (f(ip,is),ip=1,np)
	  enddo
        endif
	  do is = 1,nspin
	   do ip=1,np
            f(ip,is)=f(ip,is)-f1(ip,1)
	   enddo
	  enddo
      endif
      
      write(6,'(a,3(/,3f12.6))') 'Potential: cell =', dcell
      write(6,'(a,3i6)') 'Potential: mesh  =', mesh
      write(6,'(a,3i6)') 'Spin:    nspin   =', nspin

c Rotate cell vectors
      call rotate( 3, cell, alpha, beta, gamma )

c Find the required window limits
      xmin =  1.e30
      xmax = -1.e30
      ymin =  1.e30
      ymax = -1.e30
      do i3 = 0,1
      do i2 = 0,1
      do i1 = 0,1
        x = cell(1,1) * i1 + cell(1,2) * i2 + cell(1,3) * i3
        y = cell(2,1) * i1 + cell(2,2) * i2 + cell(2,3) * i3     
        xmin = min( x, xmin )
        xmax = max( x, xmax )
        ymin = min( y, ymin )
        ymax = max( y, ymax )
      enddo
      enddo
      enddo
      xmin = xmin - 1.
      xmax = xmax + 1.
      ymin = ymin - 1.
      ymax = ymax + 1.

c Invert potential
c      if (task.eq.'vh' .or. task.eq.'vt') then
c        do ip = 1,np
c          f(ip,2) = -f(ip,2)
c        enddo
c      endif

c Write functions to plot onto a file, and perform integration

      open( unit=8,file=foutto,status='unknown')
      close(unit=8,status='delete')
      open( unit=8,file=foutto,status='new')

      open( unit=7,file=foutav,status='unknown')
      close(unit=7,status='delete')
      open( unit=7,file=foutav,status='new')

c Average over the plane parallel to the junction
      	
	do is = 1,nspin
	 do l=1,maxort
	   aver(l,is)=0.d0
	 enddo  
	enddo  

	norm=dble(mesh(1)*mesh(2))
		
	do is = 1,nspin
	 do l=0,mesh(3)-1
	  do j=0,mesh(2)-1
           do i=0,mesh(1)-1
	    pos= 1 + i + mesh(1)*j+mesh(1)*mesh(2)*l
	    aver(l+1,is)=aver(l+1,is)+f(pos,is)
	   enddo
	  enddo  
	 enddo  
	enddo  

	do is = 1,nspin
	 do l=1,maxort
	   aver(l,is)=aver(l,is)/norm*ryd
	 enddo  
	enddo  

c One-dimensional average

	naver=int(laver/(dcell(3,3)/dble(mesh(3)))/2.d0)

	write(*,*) naver,laver

	do is = 1,nspin
	 do l=1,mesh(3)
	  daver(l,is)=0.d0
	  do i=1,2*naver
	   pos=l-1-naver+i
	    if(pos.lt.1) pos=mesh(3)+pos
	    if(pos.gt.mesh(3)) pos=pos-mesh(3)
c	    write(*,*) pos
	    daver(l,is)=daver(l,is)+aver(pos,is)
	  enddo
	 enddo  
	enddo  
	
	do is = 1,nspin
	 do l=1,maxort
	  daver(l,is)=daver(l,is)/dble(naver*2)
	 enddo 
	enddo 

c Translation if required

	ntr_shift=int(tr_shift/(dcell(3,3)/dble(mesh(3))))

         if (TR .eq. 'Y') then	 
	   call translate(maxort,aver,daver,ntr_shift,mesh(3),nspin) 
	 endif

c Write Potential

      	do per=1,period
	 do l=0,mesh(3)-1
c	  do j=0,mesh(2)-1
c           do i=0,mesh(1)-1
	     i=40
	     j=40
	    pos= 1 + i + mesh(1)*j+mesh(1)*mesh(2)*l
	    write(8,'(1i6,2f14.8)') l+1+mesh(3)*(per-1),
     .           (f(pos,is)*ryd,is=1,nspin)	
c	   enddo
c	  enddo  
	 enddo  
	enddo  

        do per=1,period
         do l=0,mesh(3)-1
          write(100,'(1i6,a3)',ADVANCE='NO') l+1+mesh(3)*(per-1),'   ' 
          do j=0,mesh(2)-1,5
           do i=0,mesh(1)-1,5
            pos= 1 + i + mesh(1)*j+mesh(1)*mesh(2)*l
            write(100,'(2f16.8)',ADVANCE='NO') 
     .       (f(pos,is)*ryd,is=1,nspin)
           enddo
          enddo
	  write(100,*)
         enddo
        enddo
            
      	do per=1,period
	 do l=2,mesh(3)
	  write(7,'(1f8.3,7f20.8)') dcell(3,3)/dble(mesh(3))
     .                           *dble(l-1)*0.5291+shift+
     .                           dcell(3,3)*0.5291*dble(per-1),
     .                           (aver(l,is)+eshift,is=1,nspin),
     .                            aver(l,1)+eshift-aver(l,2)+eshift,
     .                           (daver(l,is)+eshift,is=1,nspin),
     .                            daver(l,1)+eshift-daver(l,2)+eshift,
     .			daver(l,1)+eshift+daver(l,2)
	 enddo  
	enddo  
      	

c      integral1=integral1/176.9717497
c      integral2=integral2/176.9717497
            
        close(3)
        close(8)


      end
      
	include 'rotate.f'
	include 'translate.f'
