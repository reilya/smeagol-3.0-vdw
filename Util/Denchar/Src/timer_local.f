      subroutine timer(prog,iopt)
      character(len=*),intent(in):: prog ! Name of program to time
      integer,         intent(in):: iOpt ! Action option

!     Dummy to avoid compilation cascades

      end subroutine timer
